<?php

namespace App;

use DB;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Cmgmyr\Messenger\Traits\Messagable;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract, HasRoleAndPermissionContract
{
    use Authenticatable, CanResetPassword, HasRoleAndPermission, Messagable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'first_name', 'last_name', 'job',  'degree',  'location',  'skills',  'notes',  'sex', 'publicity'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function Plants()
    {
        return $this->hasMany('App\Plant');
    }
    public function Posts()
    {
        return $this->hasMany('App\Post');
    }
    public function Comments()
    {
        return $this->hasMany('App\Comments');
    }
    public function RelatedPosts()
    {
        $posts = $this->Posts()->get();
        foreach ($this->friends as $friend) {
            if($friend->Posts()->get()->count()>0)
                $posts = $posts->merge($friend->Posts()->get());
        }
        return $posts->sortByDesc('created_at');
    }
    
    public function friendsCount()
    {
        return $this->friends->count();
    }
    public function friends()
    {
        return $this->belongsToMany('App\User', 'friends', 'user_id', 'friend_id')
            // if you want to rely on accepted field, then add this:
            ->wherePivot('status', '=', 1)->get();
    }
    // friendship that I started
    public function friendsOfMine()
    {
        return $this->belongsToMany('App\User', 'friends', 'user_id', 'friend_id')
             ->wherePivot('status', '=', 1) // to filter only accepted
             ->withPivot('status'); // or to fetch accepted value
    }

    // friendship that I was invited to 
    public function friendOf()
    {
        return $this->belongsToMany('App\User', 'friends', 'friend_id', 'user_id')
             ->wherePivot('status', '=', 1)
             ->withPivot('status');
    }

    // accessor allowing you call $user->friends
    public function getFriendsAttribute()
    {
        if ( ! array_key_exists('friends', $this->relations)) $this->loadFriends();
        return $this->getRelation('friends');
    }

    public function friendWith($user_id)
    {
        $result = $this->belongsToMany('App\User', 'friends', 'user_id', 'friend_id')
            // if you want to rely on accepted field, then add this:
            ->wherePivot('friend_id', '=', $user_id)->get();
        return $result;
    }
    public function friendRequestWith($user_id)
    {
        $result = $this->belongsToMany('App\User', 'friends', 'user_id', 'friend_id')
            // if you want to rely on accepted field, then add this:
            ->wherePivot('status', '=', 0)->withPivot('status')->get();
        return $result;
    }

    public function friendRequestCount()
    {
        return $this->friendRequests()->count();
    }

    public function friendRequests()
    {
        return $this->belongsToMany('App\User', 'friends', 'user_id', 'friend_id')
            ->wherePivot('status', '=', 0)
            ->withPivot('status');
    }

    protected function loadFriends()
    {
        if ( ! array_key_exists('friends', $this->relations))
        {
            $friends = $this->mergeFriends();

            $this->setRelation('friends', $friends);
        }
    }

    protected function mergeFriends()
    {
        return $this->friendsOfMine->merge($this->friendOf);
    }

    public function profileImagePath()
    {
        return config("site.imagesFolder")."/".$this->profileImage_name.".jpg";
    }
}
