<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Comment extends Model
{
    protected $table = 'comments';

    public function User()
    {
    	return $this->belongsTo('App\User');
    }

    public function Post()
    {
    	return $this->belongsTo('App\Post');
    }

	public function IsOwner()
    {
    	if($this->user_id==Auth::id())
    		return true;
    	else
    		return false;
    }
}
