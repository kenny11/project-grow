<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    protected $table = 'posts';

    public function User()
    {
    	return $this->belongsTo('App\User');
    }

    public function Comments()
    {
    	return $this->hasMany('App\Comment');
    }

    public function IsOwner()
    {
    	if($this->user_id==Auth::id())
    		return true;
    	else
    		return false;
    }
}
