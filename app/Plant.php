<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Plant extends Model
{
    protected $table = 'plants';


    public function User()
    {
    	return $this->belongsTo('App\User');
    }
    public function IsOwner()
    {
    	if($this->user_id==Auth::id())
    		return true;
    	else
    		return false;
    }
    public function imagePath()
    {
        return config("site.imagesFolder")."/".$this->image_name.".jpg";
    }
}
