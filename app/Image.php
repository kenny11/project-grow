<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';

    public function User()
    {
    	return $this->belongsTo('App\User');
    }

    public function IsOwner()
    {
    	if($this->user_id==Auth::id())
    		return true;
    	else
    		return false;
    }
    public function scopeName($query, $name) {
        return $query->where('name', $name);
    }
}
