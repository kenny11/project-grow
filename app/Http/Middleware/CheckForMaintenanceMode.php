<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Routing\Middleware;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Settings;

class CheckForMaintenanceMode implements Middleware
{
	protected $request;
	protected $app;

	public function __construct(Application $app, Request $request)
	{
		$this->app = $app;
		$this->request = $request;
	}

	public function handle($request, Closure $next)
	{

		if(config('site.maintenance') == 'true')
		{
			//&& !in_array($this->request->getClientIp(), ['127.0.0.1', '10.0.0.1'])
			//Ban IP
			if(Auth::check() && Auth::user()->can('view.maintenance'))
			{
				$request->merge(array('maintenance' => 'true'));
				return $next($request);
			}
			return response()->make(view('main.maintenance'), 503);
		}
		return $next($request);
	}
}