<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Controllers\ImageController;

use App\Post;
use App\Comment;
use App\Plant;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function anyCreatePost(Request $request)
    {
        $user = Auth::user();
        $post = new Post;
        $post->user_id = $user->id;
        $post->message = $request->message;
        $post->save();
    }
    public function anyCreateImagePost(Request $request)
    {
        $user = Auth::user();
        $post = new Post;
        $post->user_id = $user->id;
        $post->message = $request->message;
        $post->data = ImageController::saveImage($request);
        $post->type = 'image';
        $post->save();
    }
    public function anyCreatePlantPost(Request $request)
    {
        $user = Auth::user();
        $post = new Post;
        $post->user_id = $user->id;
        $post->message = $request->message;
        $plant = Plant::find($request->plantId);
        if(!$request->has('plantId') || $plant == NULL || $plant->user_id != $user->id)
            return response()->json([
                'status'    => 'Error',
                'message'   => 'Plant was not found or is not yours'
                ], 404);
        $post->data = $request->plantId;
        $post->type = 'plant';
        $post->save();
        return response()->json([
            'status'    => 'Success',
            'message'   => 'Successfully shared your plant'
            ], 200);
    }
    public function anyCreateComment($id, Request $request)
    {
    	$user = Auth::user();
    	$comment = new Comment();
        try
		{
		    $post = Post::findOrFail($id);
		    $comment->post_id = $post->id;
		}
		catch(Exception $e)
		{
			return response()->json([
                'status'    => 'Error',
                'message'   => 'Post not found'
                ], 404);
		}
        $comment->user_id = $user->id;
        $comment->message = $request->message;
        $comment->save();
    }
}
