<?php
namespace App\Http\Controllers;
use App\User;

use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;

use LRedis;

use Illuminate\Http\Request;

class MessagesController extends Controller
{
	public function getThread(Request $request, $id)
	{
		try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status'    => 'Error',
                'message'   => 'Thread not found'
                ], 404);
        }
        if(!$thread->hasParticipant(Auth::id()))
        	return response()->json([
                'status'    => 'Error',
                'message'   => 'Access denied'
                ], 403);
        $thread->markAsRead(Auth::id());
        return view('messages.thread', compact('thread'));
	}
    public function getSetSeen(Request $request)
    {
        if(!$request->has('thread-id'))
            return response()->json([
                'status'    => 'Error',
                'message'   => 'Thread ID not found'
                ], 404);
        $thread;
        try {
            $thread = Thread::firstOrFail($request->input('thread-id'));
        } catch (Exception $e) {
            return response()->json([
                'status'    => 'Error',
                'message'   => 'Thread not found'
                ], 404);
        }
        $thread->markAsRead(Auth::id());
    }
	public function getMsgProto(Request $request)
	{
		$message = "";
		if($request->has('id'))
		{
			try {
				$message = Message::find($request->input('id'));
			} catch (Exception $e) {
				return "";
			}
		}
        if($request->has('markAsRead'))
            $message->thread()->markAsRead(Auth::id());
		return view('messages.components.msg-proto', compact('message'))->render();
	}
    public function getMessages(Request $request)
    {
        if(!$request->has('num') || !$request->has('offset') || !$request->has('thread'))
            return response()->json([
                'status'    => 'Error',
                'message'   => 'Too few arguments'
                ], 400);
        try {
            $thread = Thread::findOrFail($request->input('thread'));
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status'    => 'Error',
                'message'   => 'Thread not found'
                ], 404);
        }
        if(!$thread->hasParticipant(Auth::id()))
            return response()->json([
                'status'    => 'Error',
                'message'   => 'Access denied'
                ], 403);
        $num = $request->input('num');
        $offset = $request->input('offset');
        $messages = $thread->messages->reverse()->slice($offset, $num)->reverse();
        return view('messages.components.thread-messages', compact('messages'))->render();
    }
	public function sendMessage(Request $request)
	{
		$recipients = ($request->recipients ? $request->recipients : [0]);
		$thread = Thread::between(array_merge([Auth::user()->id], $recipients));
		if(!$thread->count())
			return $this->createThread($request);
		else
			return $this->addMessage($thread->first()->id, $request);

	}
    public function getList()
    {
        $currentUserId = Auth::user()->id;
        // All threads, ignore deleted/archived participants
        //$threads = Thread::getAllLatest()->get();
        // All threads that user is participating in
        $threads = Thread::forUser($currentUserId)->latest('updated_at')->get();
        // All threads that user is participating in, with new messages
        // $threads = Thread::forUserWithNewMessages($currentUserId)->latest('updated_at')->get();
        return view('messages.list', compact('threads', 'currentUserId'));
    }
    public function getAddMessage()
    {
    	$users = User::where('id', '!=', Auth::id())->get();
    	return view('messages.new', compact('users'));
    }
    /**
     * Shows a message thread.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status'    => 'Error',
                'message'   => 'Thread not found'
                ], 404);
        }
        // show current user in list if not a current participant
        // $users = User::whereNotIn('id', $thread->participantsUserIds())->get();
        // don't show the current user in list
        $userId = Auth::user()->id;
        $users = User::whereNotIn('id', $thread->participantsUserIds($userId))->get();
        $thread->markAsRead($userId);
        return view('messenger.show', compact('thread', 'users'));
    }
    /**
     * Creates a new message thread.
     *
     * @return mixed
     */
    public function create()
    {
        $users = User::where('id', '!=', Auth::id())->get();
        return view('messenger.create', compact('users'));
    }


    private function createThread()
    {
        $thread = Thread::create(
            [
                'subject' => $request->subject,
            ]
        );
        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
                'body'      => $request->message,
            ]
        );
        // Sender
        Participant::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
                'last_read' => new Carbon,
            ]
        );
        // Recipients
        if ($request->has('recipients')) {
            $thread->addParticipants($request->recipients);
        }
        return response()->json([
            'status'    => 'Success',
            'message'   => 'Successfully sent message'
            ], 201);;
    }

    private function addMessage($id, $request)
    {
    	$user = Auth::user();
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status'    => 'Error',
                'message'   => 'Thread not found'
                ], 404);
        }
        $thread->activateAllParticipants();
        $message = Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => $user->id,
                'body'      => $request->message,
            ]
        );
        // Add replier as a participant
        $participant = Participant::firstOrCreate(
            [
                'thread_id' => $thread->id,
                'user_id'   => $user->id,
            ]
        );
        $participant->last_read = new Carbon;
        $participant->save();
        // Recipients
        if ($request->has('recipients')) {
            $thread->addParticipants($request->recipients);
        }
        try {
	        $redis = LRedis::connection();
	        $tokens = array();
	        foreach ($thread->participants as $participant) {
	        	array_push($tokens, $participant->user->token);
	        }
	        $message->tokens = $tokens;
	        $rMessage = $message;
	        $redis->publish('message', $rMessage);
        } catch (Exception $e) {
        	
        }

        return response()->json([
            'status'    => 'Success',
            'message'   => 'Successfully sent message'
            ], 201);
    }
}