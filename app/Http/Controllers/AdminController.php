<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Task;
use App\User;
use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class AdminController extends Controller
{
    public function getDashboard()
    {
        return view("admin.dashboard");
    }
    public function getStatistics()
    {
    	$users = DB::table('users')->count();
    	$plants = DB::table('plants')->count();
    	return view("admin.statistics", ["users" => $users, "plants" => $plants]);
    }
    public function getUsersOverview()
    {
    	return view("admin.users.overview", ['users' => User::all()]);;
    }
    public function getUsersPermissions()
    {
    	return view("admin.users.permissions", 
            [
                'users'         => User::all()
            ]);
    }

    //API
    public function anyCreatePermission(Request $request)
    {
    	$name = $request->name;
    	$slug = $request->slug;
    	$description = $request->description;
        $table = DB::table('permissions');
        $slugCount = $table->where('slug', $slug)->count();
        $edit = $request->edit == "true" ? TRUE : FALSE;
        if($edit)
        {
            if($slugCount==0)
                return response()->json([
                    'status' => 'Error',
                    'message'=> 'Slug not found',
                    'type'   => 'permissions',
                    ], 400);
            $table
                ->where('slug', $slug)
                ->update([
                    'name'          => $name,
                    'description'   => $description
                    ]);
            return response()->json([
                'status' => 'Success',
                'message'=> 'Successfully changed permission',
                'type'   => 'permissions',
                ], 201);
        }
        if($slugCount!=0)
            return response()->json([
                'status' => 'Error',
                'message'=> 'Slug already exists',
                'type'   => 'permissions',
                ], 400);
		$createUsersPermission = Permission::create([
		    'name' => $name,
		    'slug' => $slug,
		    'description' => $description,
		]);
		return response()->json([
                'status' => 'Success',
                'message'=> 'Successfully created permission',
                'type'   => 'permissions',
                ], 201);
    }
    public function anyCreateRole(Request $request)
    {
        $name = $request->name;
        $slug = $request->slug;
        $description = $request->description;
        $table = DB::table('roles');
        $slugCount = $table->where('slug', $slug)->count();
        $edit = $request->edit == "true" ? TRUE : FALSE;
        if($edit)
        {
            if($slugCount==0)
                return response()->json([
                    'status' => 'Error',
                    'message'=> 'Slug not found',
                    'type'   => 'roles',
                    ], 400);
            $table
                ->where('slug', $slug)
                ->update([
                    'name'          => $name,
                    'description'   => $description
                    ]);
            return response()->json([
                'status' => 'Success',
                'message'=> 'Successfully changed role',
                'type'   => 'roles',
                ], 201);
        }
        if($slugCount!=0)
            return response()->json([
                'status' => 'Error',
                'message'=> 'Slug already exists',
                'type'   => 'roles',
                ], 400);
        $createUsersRole = Role::create([
            'name' => $name,
            'slug' => $slug,
            'description' => $description,
        ]);
        return response()->json([
                'status' => 'Success',
                'message'=> 'Successfully created role',
                'type'   => 'roles',
                ], 201);
    }
    public function anyCreateUser(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $description = $request->description;
        $permissions = $request->permissions;
        $roles = $request->roles;        
        $table = DB::table('users');
        $userCount = $table->where('email', $email)->count();
        $edit = $request->edit == "true" ? TRUE : FALSE;
        if($edit)
        {
            if($userCount==0)
                return response()->json([
                    'status' => 'Error',
                    'message'=> 'User not found',
                    'type'   => 'users',
                    ], 400);
            $user = User::where("email", $email)->first();
            $table
                ->where('email', $email)
                ->update([
                    'name'  => $name,
                    'email' => $email
                    ]);
            $user->detachAllRoles();
            foreach ($roles as $role) {
                if(!$user->is($role))
                    $user->attachRole(Role::where('slug', $role)->first());
            }
            $user->detachAllPermissions();
            foreach ($permissions as $permission) {
                if(!$user->can($permission))
                    $user->attachPermission(Permission::where('slug', $permission)->first());
            }
            return response()->json([
                'status' => 'Success',
                'message'=> 'Successfully changed user and his permissions and role',
                'type'   => 'users',
                ], 201);
        }
        else
        {
            if($userCount!=0)
                return response()->json([
                    'status' => 'Error',
                    'message'=> 'User already exists',
                    'type'   => 'users',
                    ], 400);
            try
            {
                $createUser = User::create([
                    'name'  => $name,
                    'email' => $email,
                ]);
                $createUser->detachAllRoles();
                $createUser->attachRole(Role::where('slug', $roles[0])->first());
                foreach ($permissions as $permission)
                {
                    $createUser->attachPermission(Permission::where('slug', $permission)->first());
                }
            }
            catch(Exception $e)
            {
                return response()->json([
                    'status' => 'Error',
                    'message'=> $e->getMessage(),
                    'type'   => 'users',
                    ], 400);
            }

            return response()->json([
                    'status' => 'Success',
                    'message'=> 'Successfully created user with permissions and role',
                    'type'   => 'users',
                    ], 201);
        }
    }

    public function anyGetPermissions(Request $request)
    {
        $column = $request->input('column');
        $data = DB::table('permissions');
        if($column != "")
            return response()->json($data->lists($column), 201);
        
        return view("admin.components.role-json",
            [
                'data'      => $data->get(),
                'last'      => $data->orderBy('id', 'desc')->first()
            ]
        );
    }

    public function anyGetRoles(Request $request)
    {
        $column = $request->input('column');
        $data = DB::table('roles');
        if($column != "")
            return response()->json($data->lists($column), 201);

        return view("admin.components.role-json",
            [
                'data'      => $data->get(),
                'last'      => $data->orderBy('id', 'desc')->first()
            ]
        );
    }

    public function anyGetPermission($id)
    {
        $data = DB::table('permissions')->where("id", $id)->first();
        if($data)
            return response()->json([
                'status'        => 'Success',
                'name'          => $data->name,
                'slug'          => $data->slug,
                'description'   => $data->description,
                ], 201);
        else
            return response()->json([
                'status'    => 'Error',
                'message'   => 'ID not found'
                ], 400);
    }

    public function anyGetRole($id)
    {
        $data = DB::table('roles')->where("id", $id)->first();
        if($data)
            return response()->json([
                'status'        => 'Success',
                'name'          => $data->name,
                'slug'          => $data->slug,
                'description'   => $data->description,
                ], 201);
        else
            return response()->json([
                'status'    => 'Error',
                'message'   => 'ID not found'
                ], 400);
    }
    public function anyGetUsers()
    {
        $users = User::all();
        $result = [];
        foreach ($users as $user)
        {
            $per = [];
            $rol = [];
            $usr = [];
            foreach ($user->getPermissions() as $permission)
            {
                array_push($per, $permission->slug);
            }
            foreach ($user->getRoles() as $role)
                array_push($rol, $role->slug);

            array_push($usr, $user->id);
            array_push($usr, $user->name);
            $row = [];
            $row["permissions"] = $per;
            $row["roles"] = $rol;
            $row["user"] = $usr;
            array_push($result, $row);
        }
        $temp["data"] = $result;
        return json_encode($temp);
    }
    public function anyGetUser($id)
    {
        $user = User::where("id", $id)->first();
        return response()->json([
            'status'        => 'Success',
            'name'          => $user->name,
            'email'          => $user->email,
            'permissions'   => $user->getPermissions(),
            'roles'         => $user->getRoles(),
            ], 201);
    }
    public function anyRemovePermission($id)
    {
        $request = DB::table('permissions');
        $idCount = $request->where('id', $id)->count();
        if($idCount==0)
            return response()->json([
                'status' => 'Error',
                'message'=> 'ID not found',
                'type'   => 'permissions',
                ], 400);
        $request->where('id', $id)->delete();
        return response()->json([
            'status' => 'Success',
            'message'=> 'Successfully removed permission',
            'type'   => 'permissions',
            ], 201);

    }
    public function anyRemoveRole($id)
    {
        $request = DB::table('roles');
        $idCount = $request->where('id', $id)->count();
        if($idCount==0)
            return response()->json([
                'status' => 'Error',
                'message'=> 'ID not found',
                'type'   => 'roles',
                ], 400);
        $request->where('id', $id)->delete();
        return response()->json([
            'status' => 'Success',
            'message'=> 'Successfully removed role',
            'type'   => 'roles',
            ], 201);
    }
    public function anyRemoveUser($id)
    {
        $request = DB::table('users');
        $user = User::where("id", $id)->first();
        $user->detachAllRoles();
        $user->detachAllPermissions();
        $idCount = $request->where('id', $id)->count();
        if($idCount==0)
            return response()->json([
                'status' => 'Error',
                'message'=> 'ID not found',
                'type'   => 'users',
                ], 400);
        $request->where('id', $id)->delete();
        return response()->json([
            'status' => 'Success',
            'message'=> 'Successfully removed user',
            'type'   => 'users',
            ], 201);
    }
}
