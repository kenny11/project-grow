<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use App\Http\Controllers\ImageController;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;

class RouteController extends Controller
{
	public function mainPage()
	{
		if(Auth::check())
			return view('main.feed');
		else
			return view('main.welcome');
	}
	public function crypto()
	{
		return view('develop.crypto');
	}
}