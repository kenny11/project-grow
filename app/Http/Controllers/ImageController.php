<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Image;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ImageController extends Controller
{

    public function getImage($name)
    {
        $path = str_replace('\\', '/', public_path()."/".config('site.imagesFolder')."/".$name);
        //r(!strpos($name, ".jpg"));
        if(file_exists($path.".jpg") || file_exists($path))
        {
            //r(strpos($name, ".jpg"));
            header("Content-Type: image/jpg");
            if(!strpos($name, ".jpg"))
                return readfile(public_path()."\\images\\".$name);
            else
                return readfile(public_path()."\\images\\".$name.".jpg");
        }
        else
            return response()->json([
                'status'    => 'Error',
                'message'   => 'Image not found'
                ], 404);
    }
    public static function saveImage(Request $request, $returnType = "name", $crop = "none")
    {
        function generateRandomString($length = 10) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }
        $temp_name = generateRandomString();
        while (Image::name($temp_name)->count()) {
            $temp_name = generateRandomString();
        }
        $imagesFolder = config('site.imagesFolder');

        if (!$request->hasFile('image') || !$request->file('image')->isValid()) 
        {
            return response()->json([
                'status'    => 'Error',
                'message'   => 'Image is not valid'
                ], 400);
        }
        $validator = Validator::make($request->all(), 
            [
                'image' => 'required|mimes:jpeg',
            ]);
        if($validator->fails())
            return response()->json([
                'status'    => 'Error',
                'message'   => 'Image is in wrong format',
                ], 400);
        $user = Auth::user();
        $image = new Image();
        $image->user_id = $user->id;
        $image->name = $temp_name;
        $image->save();
        $imagePath;
        try
        {
            $imagePath = $request->file('image')->move($imagesFolder, $image->name.".jpg");
        }
        catch (Exception $e)
        {
            return response()->json([
                'status'    => 'Error',
                'message'   => 'Image is in wrong format',
                ], 400);
        }
        if($returnType=="name")
            return str_replace('\\', '/', $imagePath);
        return $image->name;
    }
    public function showUpload()
    {
        return view("develop.image");
    }
}
