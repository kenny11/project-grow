<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;

use App\Plant;
use App\Image;

use Illuminate\Support\Facades\Auth;

class PlantController extends Controller
{
    public function getList()
    {
    	if(Auth::check() && Auth::user()->can('plant.list'))
        	return view('plants.list');
        else 
        	return response()->json([
                'status'    => 'Error',
                'message'   => 'Access denied'
                ], 403);
    }
    public function getAdd()
    {
    	if(Auth::check() && Auth::user()->can('plant.add'))
        	return view('plants.add');
        else 
        	return response()->json([
                'status'    => 'Error',
                'message'   => 'Access denied'
                ], 403);
    }
    public function postAdd(Request $request)
    {
    	if(Auth::check() && Auth::user()->can('plant.add'))
    	{
        	$user = $request->user();
        	$image = ImageController::saveImage($request, "id");
        	$plant = new Plant();
        	$plant->user_id = $user->id;
        	$plant->name = $request->name;
        	$plant->description = $request->description;
        	$plant->image_name = $image;
        	$plant->color = $request->color;
        	$plant->save();
			return "";
    	}
        else 
        	return response()->json([
                'status'    => 'Error',
                'message'   => 'Access denied'
                ], 403);
    }
    public function anyUpdate(Request $request)
    {
        $plant = "";
        try {
            $plant = Plant::find($request->plant_id);
        } catch (Exception $e) {
            return response()->json([
                'status'    => 'Error',
                'message'   => 'Plant not found'
                ], 404);
        }

        if(!$plant->user_id == Auth::id())
            return response()->json([
                'status'    => 'Error',
                'message'   => 'Plant not found'
                ], 404);
        if($request->imagename == $plant->image)
        {
            $image = ImageController::saveImage($request, "id");
            $plant->image_name = $image;
        }
        $plant->name = $request->name;
        $plant->description = $request->description;
        $plant->color = $request->color;
        $plant->save();
        return redirect()->back();
    }
    public function getDetails($id)
    {
    	$plant = Plant::find($id);
    	if($plant !="")
    	{
			return view('plants.details', ['plant' => $plant, 'owner' => $plant->IsOwner()]);
    	}
    	else
    		return response()->json([
                'status'    => 'Error',
                'message'   => 'Plant not found'
                ], 404);
    }    
}
