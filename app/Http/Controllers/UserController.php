<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ImageController;
use DB;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function findById($id)
    {
        return view('user.profile', ['user' => User::find($id)]);
    }

    public function showProfile()
    {
        return view('user.profile', ['user' => 'self']);
    }
    public function getFriends()
    {
    	return view('user.friends');
    }
    public function postFriends(Request $request)
    {
        if($request->has('action') && $request->has('id'))
        {
            $id = $request->input('id');
            switch($request->input('action'))
            {
                case 'add':
                    return $this->getAddFriend($id);
                    break;
                case 'remove':
                    return $this->getRemoveFriend($id);
                    break;
                case 'accept':
                    return $this->getAcceptFriend($id);
                    break;
            }
        }
        return;
    }
    public function getAddFriend($id)
    {
        $quarry = DB::table('friends')->where('user_id', '=', Auth::user()->id)->where('friend_id', '=', $id);
        $quarry2 = DB::table('friends')->where('friend_id', '=', Auth::user()->id)->where('user_id', '=', $id);
        $quarry = $quarry->count()>$quarry2->count()?$quarry:$quarry2;
        if(!$quarry->count())
        {
            try 
            {
                $dt = new \DateTime;
                $now = $dt->format('m-d-y H:i:s');
                DB::table('friends')->insert(
                    ['user_id' => $id, 'friend_id' => Auth::user()->id, 'status' => '0', 'created_at' => $now, 'updated_at' => $now]
                );
                return response()->json([
                    'status'    => 'Success',
                    'message'   => 'Successfully sent friend request'
                    ], 201);
            } 
            catch (Exception $e) 
            {
                return response()->json([
                    'status'    => 'Error',
                    'message'   => 'Error while accessing database'
                    ], 400);
            }
        }
        if(!$quarry->first()->status)
            return response()->json([
                'status'    => 'Error',
                'message'   => 'Friend request already sent'
                ], 400);
        return response()->json([
            'status'    => 'Error',
            'message'   => 'Users are already friends'
            ], 400);
    }
    public function getRemoveFriend($id)
    {
        $quarry = DB::table('friends')->where('user_id', '=', Auth::user()->id)->where('friend_id', '=', $id);
        $quarry2 = DB::table('friends')->where('friend_id', '=', Auth::user()->id)->where('user_id', '=', $id);
        $quarry = $quarry->count()>$quarry2->count()?$quarry:$quarry2;
        if($quarry->count())
        {
            try 
            {
                $quarry->delete();
                return response()->json([
                    'status'    => 'Success',
                    'message'   => 'Successfully removed friend'
                    ], 201);
            } 
            catch (Exception $e) 
            {
                return response()->json([
                    'status'    => 'Error',
                    'message'   => 'Error while accessing database'
                    ], 400);
            }
        }
        return response()->json([
            'status'    => 'Error',
            'message'   => 'Users are not friends'
            ], 400);
    }
    public function getAcceptFriend($id)
    {
        $quarry = DB::table('friends')->where('user_id', '=', Auth::user()->id)->where('friend_id', '=', $id)->where('status', '=', '0');
        $quarry2 = DB::table('friends')->where('friend_id', '=', Auth::user()->id)->where('user_id', '=', $id)->where('status', '=', '0');
        $quarry = $quarry->count()>$quarry2->count()?$quarry:$quarry2;
        if(!$quarry->count())
            return response()->json([
                'status'    => 'Error',
                'message'   => 'No friend request'
                ], 400);
        try 
        {
            $quarry->update(['status' => '1']);
            return response()->json([
                    'status'    => 'Success',
                    'message'   => 'Successfully confirmed request'
                    ], 201);
        } 
        catch (Exception $e) {
            return response()->json([
                'status'    => 'Error',
                'message'   => 'Error while accessing database'
                ], 400);
        }

    }
    public function changeProfile(Request $request)
    {
        if(!$request->has('editType'))
            return response()->json([
                'status'    => 'Error',
                'message'   => 'Request is not valid'
                ], 400);
        $user = Auth::user();
        switch ($request->input('editType')) 
        {
            case 'profileImage':
                $this->profileImage($request);
                return response()->json([
                    'status'    => 'Success',
                    'message'   => 'Successfully changed profile image',
                    'data'      => $user->profileImage_name,
                    ], 201);
                break;
            case 'profileInfo':
                $this->profileInfo($request);
                return response()->json([
                    'status'    => 'Success',
                    'message'   => 'Successfully changed profile info'
                    ], 201);
                break;            
            default:
                return response()->json([
                    'status'    => 'Error',
                    'message'   => 'Request is not valid'
                    ], 400);
                break;
        }
    }
    public function getIdFromToken(Request $request)
    {
        if(!$request->has('token'))
            return "0";
        $id = 0;
        $id = User::where('token', $request->input('token'))->id;
        return $id;
    }
    private function profileImage(Request $request)
    {
        $user = Auth::user();
        $image = ImageController::saveImage($request, "id");
        $user->profileImage_name = $image;
        $user->save();
    }
    private function profileInfo(Request $request)
    {
        $user = Auth::user();
        foreach ($request['info'] as $type => $info) 
        {
            $user->$type = $info;
        }
        $user->save();
    }
}
