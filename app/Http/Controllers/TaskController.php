<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    public function getDevelop()
    {
        return view("admin.develop", ['tasks' => Task::all()]);
    }
    public function anyCreateTask(Request $request)
    {
        try
        {
            $task = Task::create([
                    'name'      =>  $request->name,
                    'progress'  =>  $request->progress,
                    'priority'  =>  $request->priority,
                ]);
            return response()->json([
                'status' => 'Success',
                'message'=> 'Successfully created task',
                'type'   => 'task',
                ], 201);
        }
        catch(Exception $e)
        {
            return response()->json([
                'status' => 'Error',
                'message'=> $e->getMessage(),
                'type'   => 'task',
                ], 400);
        }
    }
}
