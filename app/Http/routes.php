<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'RouteController@mainPage');
Route::get('/en', function () {
    App::setLocale('en');
    return redirect('/');
});
Route::get('/cs', function () {
    App::setLocale('cs');
    return redirect('/');
});
//Route::get('/dev/friend', function () {return view('develop.friend');});// Friends debug
Route::get('/dev/message', 'MessagesController@getMsgProto');
Route::get('/dev/send', 'MessagesController@sendMessage');
Route::get('/dev/crypto', 'RouteController@crypto');



Route::group(['prefix' => 'friends'], function(){
	Route::get('/', 'UserController@getFriends');
	Route::post('/', 'UserController@postFriends');
	Route::get('/add/{id}', 'UserController@getAddFriend');
	Route::get('/remove/{id}', 'UserController@getRemoveFriend');
	Route::get('/accept/{id}', 'UserController@getAcceptFriend');
});

Route::any('image/{id}', 'ImageController@getImage');
Route::get('uploadImage', 'ImageController@showUpload');
Route::post('uploadImage', 'ImageController@saveImage');

//TODO: Complet clean
Route::get('/old', function () {
    return view('main.components.app');
});


Route::get('/m/{id}', 'MessagesController@getThread');
Route::post('api/m/msg-proto', 'MessagesController@getMsgProto');
Route::post('api/m/thread-messages', 'MessagesController@getMessages');
Route::group(['prefix' => 'messages'], function(){
	Route::get('/', 'MessagesController@getList');
	Route::post('/', 'MessagesController@sendMessage');
});

Route::group(['prefix' => 'plant'], function(){
	Route::get('/', 'PlantController@getList');
	Route::get('/add', 'PlantController@getAdd');
	Route::post('/add', 'PlantController@postAdd');
	Route::any('/update', 'PlantController@anyUpdate');
	Route::get('list', 'PlantController@getList');
	Route::post('list', 'PlantController@postList');
	Route::get('/{id}', 'PlantController@getDetails');
});

Route::group(['prefix' => 'user'], function (){
	Route::post('changeProfile', 'UserController@changeProfile');
	Route::post('/get/fromToken', 'UserController@getIdFromToken');
});

Route::group(['prefix' => 'post'], function () {
	Route::any('create', ['middleware' => 'permission:post.create', 'uses' => 'PostController@anyCreatePost']);
	Route::any('image', ['middleware' => 'permission:post.create', 'uses' => 'PostController@anyCreateImagePost']);
	Route::any('plant', ['middleware' => 'permission:post.create', 'uses' => 'PostController@anyCreatePlantPost']);
	Route::any('/{id}/comment/create', ['middleware' => 'permission:post.comment', 'uses' => 'PostController@anyCreateComment']);
});

//Administration
Route::group(['prefix' => 'admin'], function () {
	Route::get('/', function () { return redirect('admin/dashboard'); });
	Route::get('dashboard', ['middleware' => 'permission:admin.dashboard', 'uses' => 'AdminController@getDashboard']);
	Route::get('statistics', ['middleware' => 'permission:admin.statistics', 'uses' => 'AdminController@getStatistics']);	
	Route::get('develop', ['middleware' => 'permission:admin.develop', 'uses' => 'TaskController@getDevelop']);
	Route::group(['prefix' => 'users'], function () {
		Route::get('/', function () { return redirect('admin/users/overview'); });
		Route::get('overview', ['middleware' => 'permission:admin.users.overview', 'uses' => 'AdminController@getUsersOverview']);
		Route::get('permissions', ['middleware' => 'permission:admin.users.permissions', 'uses' => 'AdminController@getUsersPermissions']);
	});
	Route::group(['prefix' => 's'], function () {
		Route::any('gender', ['middleware' => 'permission:admin.statistics', 'uses' => 'StatisticsController@genderStatistics']);
	});
	//API
	//Get
	Route::group(['prefix' => 'get'], function () {
		Route::any('permissions', ['middleware' => 'permission:admin.users.permissions', 'uses' => 'AdminController@anyGetPermissions']);
		Route::any('permission/{id}', ['middleware' => 'permission:admin.users.permissions', 'uses' => 'AdminController@anyGetPermission']);
		Route::any('roles', ['middleware' => 'permission:admin.users.permissions', 'uses' => 'AdminController@anyGetRoles']);
		Route::any('role/{id}', ['middleware' => 'permission:admin.users.permissions', 'uses' => 'AdminController@anyGetRole']);
		Route::any('users', ['middleware' => 'permission:admin.users.permissions', 'uses' => 'AdminController@anyGetUsers']);
		Route::any('user/{id}', ['middleware' => 'permission:admin.users.permissions', 'uses' => 'AdminController@anyGetUser']);
	});
	//Create/edit
	Route::group(['prefix' => 'create'], function () {
		Route::any('permission', ['middleware' => 'permission:admin.users.permissions', 'uses' => 'AdminController@anyCreatePermission']);
		Route::any('role', ['middleware' => 'permission:admin.users.permissions', 'uses' => 'AdminController@anyCreateRole']);
		Route::any('user', ['middleware' => 'permission:admin.users.permissions', 'uses' => 'AdminController@anyCreateUser']);
		Route::any('task', ['middleware' => 'permission:admin.users.develop', 'uses' => 'TaskController@anyCreateTask']);
	});
	//Remove
	Route::group(['prefix' => 'remove'], function (){
		Route::any('permission/{id}', ['middleware' => 'permission:admin.users.permissions', 'uses' => 'AdminController@anyRemovePermission']);
		Route::any('role/{id}', ['middleware' => 'permission:admin.users.permissions', 'uses' => 'AdminController@anyRemoveRole']);
		Route::any('user/{id}', ['middleware' => 'permission:admin.users.permissions', 'uses' => 'AdminController@anyRemoveUser']);		
	});
});



//Route::get('profile', ['middleware' => 'auth', 'uses' => 'UserController@showProfile']);
Route::get('/{id}', 'UserController@findById');

//Auth
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);



