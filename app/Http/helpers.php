<?php 
use App\Post;
use App\User;
use Cmgmyr\Messenger\Models\Thread;

class Helper
{
	public static function dateToText($date) {
		$date = new DateTime($date);
		$now = new DateTime();
		$interval = $date->diff($now);
		if($interval->y > 0)
			return Lang::choice('site/post.time-year', $interval->y,['year' => $interval->y]);
		elseif($interval->m > 0)
			return Lang::choice('site/post.time-month', $interval->m,['month' => $interval->m]);
		elseif($interval->d > 7 )
			return Lang::choice('site/post.time-week', $interval->d % 7,['week' => $interval->d % 7]);
		elseif($interval->d > 0)
			return Lang::choice('site/post.time-day', $interval->d,['day' => $interval->d]);
		elseif($interval->h > 0)
			return Lang::choice('site/post.time-hour', $interval->h, ['hour' => $interval->h]);
		elseif($interval->i > 0)
			return Lang::choice('site/post.time-minute', $interval->i, ['minute' => $interval->i]);
		else return Lang::choice('site/post.time-seconds', $interval->s, ['second' => $interval->s]);
	}

	public static function setDisabled($name)
	{
	    return (Auth::check() && Auth::user()->can(str_replace('/', '.', $name)))? "" : "disabled";
	}

	public static function setActive($name)
	{
	    return (URL::getRequest()->is($name))? "active" : "";
	}

	public static function getAllPosts()
	{
	    return Post::all();
	}

	public static function isUserSame($other_user)
	{
		if(!Auth::check())
			return false;
		if(is_numeric($other_user))
			return Auth::user()->id == $other_user;
		return Auth::user()->id == $other_user->id;
	}
	public static function profileImagePath()
    {
		return "images/".Auth::user()->profileImage_name.".jpg";
    }
    public static function isDownForMaintenance()
    {
    	return config('site.maintenance');
    }
    public static function getUser($id)
    {
    	return User::find($id);
    }
    public static function threadName($thread, $onlyName = False)
    {
    	if($thread->subject!="")
			return $thread->subject;
		else
		{
			$response = "";
			$i = 0;
			foreach($thread->participants as $participant)
			{
				if(!($participant->user_id == Auth::user()->id))
				{
					$pUser = Helper::getUser($participant->user_id); 
					$i++;
					$name = Helper::userFullName($pUser);
					if($onlyName)
						$response .= $name;
					else
						$response .= "<a href='".URL::to($pUser->id)."' title='$name'>$name</a>";
					if($i<$thread->participants->count()-1)
						$response .= ", ";
				}
			}
			return $response;
		}
    }
    public static function getThread($id)
    {
    	return Thread::findOrFail($id);
    }
    public static function userFullName($user)
    {
    	return $user->first_name."&nbsp;".$user->last_name;
    }
    public static function getPlant($id)
    {
    	return App\Plant::findOrFail($id);
    }
}
