var visitData = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
        {
            label: "Women",
            fillColor: "transparent",
            strokeColor: "#EC87C0",
            pointColor: "#D770AD",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: [1, 2, 6, 14, 17, 26, 38]
        },
        {
            label: "Men",
            fillColor: "transparent",
            strokeColor: "#5D9CEC",
            pointColor: "#4A89DC",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: [2, 5, 11, 14, 18, 24, 32]
        }
    ]
};
