@extends('admin.components.app')
<?php $page_title = trans('site/admin.permissions_page'); $formNumber = 0; $tables = array();?>
@section('meta_x-csfr')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset ("/bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.css") }}">
<link rel="stylesheet" type="text/css" href="{{ asset ("/bower_components/admin-lte/plugins/select2/select2.min.css") }}">
<style type="text/css" media="screen">
	table { table-layout: fixed; overflow: hidden; }
	table td { word-wrap: break-word; }
	table tr { max-width: 95%; }
	.select2-container--default .select2-selection--multiple .select2-selection__choice { color: #333;}
</style>		
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
            </div>
            <div class="modal-body">
                <p>Do you really want to remove it?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class='col-md-12'>
		<div class="box box-solid box-default" id="StatusBox" style="display: none;">
			<div class="box-header with-border">
				<h3 class="box-title">Removable</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div><!-- /.box-tools -->
			</div><!-- /.box-header -->
			<div class="box-body">
				The body of the box
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div>
</div>		
    <div class='row'>
		<div class='col-md-7'> <!-- Tabulky -->
			<?php $tableID = "permissions"; $tableTitle = "Permissions"; array_push($tables, $tableID);?>
			@include('admin.components.table')
		</div><!-- /.col -->
		<div class='col-md-5'> <!-- Formulare -->
			<?php $formID = "permissionsForm"; $formTitle = "Create or edit permission"; $formNumber++; ?>
			@include('admin.components.form')
		</div> <!-- Konec formularu -->
	</div>
	<div class='row'>
		<div class='col-md-7'> <!-- Tabulky -->
			<?php $tableID = "roles"; $tableTitle = "Roles"; array_push($tables, $tableID);?>
			@include('admin.components.table')
		</div>
		<div class='col-md-5'> <!-- Formulare -->
			<?php $formID = "rolesForm"; $formTitle = "Create or edit role"; $formNumber++; ?>
			@include('admin.components.form')
		</div>
	</div>
	<div class='row'>
		<div class='col-md-7'> <!-- Tabulky -->
			<?php $tableID = "users"; $tableTitle = "User roles and permissions"; array_push($tables, $tableID);?>
			@include('admin.components.table-spec')
		</div>
		<div class='col-md-5'> <!-- Formulare -->
			<?php $formID = "usersForm"; $formTitle = "Create or edit user"; $formNumber++; ?>
			@include('admin.components.form-spec')
		</div>
	</div>
	<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.js") }}"></script>
	<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.js") }}"></script>
	<script src="{{ asset ("/bower_components/admin-lte/plugins/select2/select2.full.min.js") }}"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		//Global variables
		var datatables = [];
		var select2 = [];
		var deleteID;
		var timeout;

		//Initialize tables
		@foreach ($tables as $t)
			@if ($t == "users")
				datatables.{{$t}} = setupSpecialTable("{{$t}}");
			@else
	    		datatables.{{$t}} = setupTable("{{$t}}");
    		@endif
    	@endforeach
		//Initialize forms
    	prepareSelect2("Permissions");
    	prepareSelect2("Roles");
    	//Record controll buttons
	    $(".special tbody").on('click', 'a', function(){
	    	var data = $(this).attr('value').split("-");
	    	var form = data[0];
	    	var action = data[1];
	    	var id = data[2];
	    	if(action == "remove")
	    		deleteID = [form, id];	    		
	    	if(action=="edit")
	    		timeout = setInterval(setFormData(form, id), 10000);
	    });
	    //Delete confirmation (modal)
 		$('#confirm-delete').on('click', function(e) {
            e.preventDefault();
            $("#confirm-delete").modal('hide');
            deleteRow();
        });

 		//Delete record from database and reload table
 		function deleteRow()
 		{
 			var type = deleteID[0].substring(0, deleteID[0].length - 1);
 			var id = deleteID[1];
			$.ajax({
				type: "POST",
				url: "../remove/"+type+"/"+id
			})  
			.done(function(json) {
				ShowBox(json.message ,json.status , "success" );
				datatables[json.type].ajax.reload();
			})
			.fail(function(data) {
				var json = $.parseJSON(data.responseText);
				ShowBox(json.message ,json.status , "danger" );
			});
 		}

 		//Get and insert data to forms
	    function setFormData(form, id)
	    {
	    	$("#"+form+"FormOverlay").show();
    		$.when(getAjaxObject(form, id)).then(function( data, textStatus, jqXHR ) {
	    		clearInterval(timeout);
	    		if(form == "users")
	    		{
	    			var permissions = [], roles = [];
	    			$.each(data.permissions, function( index, value ) {
						permissions.push(value.slug);
					});
					$.each(data.roles, function( index, value ) {
						roles.push(value.slug);
					});
	    			$("#"+form+"Form input[name=name]").val(data.name);
	    			$("#"+form+"Form input[name=email]").val(data.email);
			  		select2.Permissions.val(permissions).trigger("change");
			  		select2.Roles.val(roles).trigger("change");
			  		$("#"+form+"Form input[name=edit]").prop('checked', true);
	    		}
	    		else
	    		{
			  		$("#"+form+"Form input[name=name]").val(data.name);
	    			$("#"+form+"Form input[name=slug]").val(data.slug);
		  			$("#"+form+"Form input[name=description]").val(data.description);
		  			$("#"+form+"Form input[name=edit]").prop('checked', true);
	    		}
	    		$("#"+form+"FormOverlay").hide();
			});
	    }

	    //Setup dataTable
	    function setupTable(name)
	    {
	    	return $('#'+name).DataTable({
		    	"processing": true,
		    	"serverSide": false,
		    	"ajax" : "../get/"+name,
				"columns" : [
					{'data':'0'},
					{'data':'1'},
					{'data':'2'},
					{'data':'3'},
					{
						sortable: false,
						"render": function ( data, type, full, meta ) {
							return '\
							<div class="btn-group btn-group-xs controllButtons">\
								<a class="btn btn-warning" role="button" value="'+name+'-edit-'+full[0]+'">\
									<i class="fa fa-pencil"></i>\
								</a>\
								<a class="btn btn-danger" role="button" value="'+name+'-remove-'+full[0]+'" data-toggle="modal" data-target="#confirm-delete">\
									<i class="fa fa-times"></i>\
								</a>\
							</div>';
						}
					}
				]
		    });
	    }
	    //Special table for user permissions and roles
		function setupSpecialTable(name)
		{
			return $('#'+name).DataTable({
				"autoWidth": true,
				"processing": true,
		    	"ajax" : "../get/"+name,
		    	"fnInitComplete": function() {
				    datatables["users"].columns.adjust().draw();
				},
				"columns" : [
					{'data':'user.0'},
					{'data':'user.1'},
					{'data':'permissions'},
					{'data':'roles'},
					{
						sortable: false,
						"render": function ( data, type, full, meta ) {
							return '\
							<div class="btn-group btn-group-xs controllButtons">\
								<a class="btn btn-warning" role="button" value="'+name+'-edit-'+full["user"][0]+'">\
									<i class="fa fa-pencil"></i>\
								</a>\
								<a class="btn btn-danger" role="button" value="'+name+'-remove-'+full["user"][0]+'" data-toggle="modal" data-target="#confirm-delete">\
									<i class="fa fa-times"></i>\
								</a>\
							</div>';
						}
					}
				]
			})
		}

	    //Get object(s) from server via ajax
	    function getAjaxObject(object, id = "")
	    {
	    	var idURL = "";
	    	var result = "";
	    	if(id!="")
	    	{
	    		idURL = "/"+id;
	    		object = object.substring(0, object.length - 1);
	    	}
	    	return $.ajax({
				type: "GET",
				url: "../get/"+object+""+idURL,
			});
	    }
	    //X-CSRF token setup
		$.ajaxSetup({
	        headers: 
	        {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
		});

	    

		function prepareSelect2(name)
		{
			if(name == "Roles")
				select2[name] = $("#input"+name+"3").select2({
					tags: true,
					multiple: true,
					maximumSelectionLength: 1,
					quietMillis: 300,
					tokenSeparators: [',', ' ']
				});
			else
				select2[name] = $("#input"+name+"3").select2({
					tags: true,
					multiple: true,
					quietMillis: 300,
					tokenSeparators: [',', ' ']
				});
			$.when(getAjaxObject(name.toLowerCase()+".", "?column=slug")).then(function( data, textStatus, jqXHR ) {
				$("#input"+name+"3").empty();
				$.each( data, function( key, value ) {
					$("#input"+name+"3").append("<option value="+value+">"+value+"</option>")
				});
			});
		}

		

	    //Sends ajax request(save or edit)
		$('form.form-horizontal').on( 'submit', function(e) {
			e.preventDefault();
			var id = $(this).context.id;
			var name = $("#"+id+" input[name=name]").val();
			var slug = $("#"+id+" input[name=slug]").val();
			var description = $("#"+id+" input[name=description]").val();

			var permissions = select2.Permissions.val();
			var roles = select2.Roles.val();
			var email = $("#"+id+" input[name=email]").val();

			var edit = $("#"+id+" input[name=edit]").prop('checked');

			if(description == "")
				description = name;
			var ajax;
			if(permissions != "" && permissions != null)
				ajax = $.ajax({
					type: "POST",
					url: e.target.action,
					data: {name:name, email:email, permissions:permissions, roles: roles, edit: edit}
				}) 
			else
				ajax = $.ajax({
					type: "POST",
					url: e.target.action,
					data: {name:name, slug:slug, description:description, edit: edit}
				})  
			ajax.done(function(json) {
				ShowBox(json.message ,json.status , "success" );
				datatables[json.type].ajax.reload();
				prepareSelect2(json.type.charAt(0).toUpperCase() + json.type.slice(1));
			})
			.fail(function(data) {
				var json = $.parseJSON(data.responseText);
				ShowBox(json.message ,json.status , "danger" );
			});
		});
		//Show informational box
		function ShowBox(msg, header, type="success") {
			$("#StatusBox").removeClass();
			$("#StatusBox").addClass("box box-solid box-" + type);
			$("#StatusBox .box-body").text(msg);
			$("#StatusBox .box-header .box-title").text(header);
			$("#StatusBox").slideDown();
		};
	});
	</script>
@endsection
