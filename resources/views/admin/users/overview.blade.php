@extends('admin.components.app')
<?php $page_title = trans('site/admin.users_overview_page'); ?>
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset ("/bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.css") }}">
    <div class='row'>
        <div class='col-xs-12'>
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Data Table With Full Features</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
				        <thead>
				            <tr>
				                <th>ID</th>
				                <th>Name</th>
				                <th>Email</th>
				                <th>Creation date</th>
				                <th>Update date</th>
				            </tr>
				        </thead>
				        <tfoot>
				            <tr>
				                <th>ID</th>
				                <th>Name</th>
				                <th>Email</th>
				                <th>Creation date</th>
				                <th>Update date</th>
				            </tr>
				        </tfoot>
				        <tbody>
					        @foreach ($users as $user)
					            <tr>
					                <td>{{$user->id}}</td>
					                <td>{{$user->name}}</td>
					                <td>{{$user->email}}</td>
					                <td>{{$user->created_at}}</td>
					                <td>{{$user->updated_at}}</td>
					            </tr>
				            @endforeach
				        </tbody>
				    </table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div>
	<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.js") }}"></script>
	<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.js") }}"></script>
	<script type="text/javascript">
	$(document).ready(function() {
	    $('#example').DataTable();
	} );
	</script>
@endsection