@extends('admin.components.app')
<?php $page_title = trans('site/admin.dev_page'); $formNumber = 0; ?>
@section('content')
<div class='row'>
	<div class='col-md-6'>
		<div class="box box-solid box-info">
			<div class="box-header">
			<h3 class="box-title">Tasks</h3>
			</div>
			<div class="box-body">
                @foreach($tasks as $task)
                    <h5>
                        {{ $task['name'] }}
                        <small class="label label-{{$task['priority']}} pull-right">{{$task['progress']}}%</small>
                    </h5>
                    <div class="progress progress-xxs">
                        <div class="progress-bar progress-bar-{{$task['priority']}}" style="width: {{$task['progress']}}%"></div>
                    </div>
                @endforeach
			</div>
		</div>
	</div>
	<div class='col-md-5'> <!-- Formulare -->
		<div class="box box-solid box-primary">
			<div class="box-header">
				<h3 class="box-title">Create task</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<form class="form-horizontal" id="taskForm" action="{{URL::to('admin/create/task')}}" method="POST">
					<input type="hidden" name="textik" value="false">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="inputName">
							Name
						</label>
						<div class="col-sm-10">
							<input id="inputName" class="form-control" type="text" placeholder="Name" name="name" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="inputProgress">
							Progress
						</label>
						<div class="col-sm-10">
							<input id="inputProgress" class="form-control" type="text" placeholder="0" name="progress" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="inputPriority">
							Priority
						</label>
						<div class="col-sm-10">
							<select class="form-control" id="inputPriority" class="form-control" name="priority">
							    <option>Normal</option>
							    <option>Default</option>
							    <option>Primary</option>
							    <option>Success</option>
							    <option>Info</option>
							    <option>Warning</option>
							    <option>Danger</option>
						  	</select>
						</div>        
					</div>
					<div class="box-footer">
						<div class="col-sm-9">
							
						</div>
						<div class="col-sm-3">
							<button class="btn btn-info pull-right" type="submit">Add</button>
						</div>
					</div>
				</form>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div>
</div>

@endsection