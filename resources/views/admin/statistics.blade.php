@extends('admin.components.app')
<?php $page_title = trans('site/admin.statistics_page'); ?>
@section('content')
    <div class='row'>
    	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Gender</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
					</div>
				</div>
				<div class="box-body">
					<canvas id="genderChart" height="263px" width="527px"></canvas>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Visits</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
					</div>
				</div>
				<div class="box-body">
					<canvas id="visitChart" height="263px" width="527px"></canvas>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<i class="fa fa-bar-chart-o"></i>
					<h3 class="box-title">Server usage</h3>
					<div class="box-tools pull-right">
						Real time
						<div class="btn-group" id="realtime" data-toggle="btn-toggle">
							<button type="button" class="btn btn-default btn-xs active" data-toggle="on">On</button>
							<button type="button" class="btn btn-default btn-xs" data-toggle="off">Off</button>
						</div>
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
					</div>
				</div>
				<div class="box-body">
				<div id="interactive" style="height: 300px;"></div>
				</div>
				<!-- /.box-body-->
			</div>
		</div>		
    </div>
    <script src="{{ asset ("/bower_components/admin-lte/plugins/chartjs/Chart.min.js") }}"></script>
    <script src="{{ asset ("/bower_components/admin-lte/plugins/flot/jquery.flot.min.js") }}"></script>
	<script type="text/javascript">
		var genderChart;
		$.ajaxSetup(
		{
		    headers:
		    {
		        'X-CSRF-Token': $('input[name="_token"]').val()
		    }
		});
		var settings = {
		    segmentShowStroke : true,
		    segmentStrokeColor : "#fff",
		    segmentStrokeWidth : 2,
		    percentageInnerCutout : 50, // This is 0 for Pie charts
		    animationSteps : 100,
		    animationEasing : "easeOutBounce",
		    animateRotate : true,
		    animateScale : false,
		    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
		}

		$.ajax({
			type: "POST",
			url: "{{URL::to('admin/s/gender')}}"
		})  
		.done(function(json) {
			var val = JSON.parse(json.data);
			var ctx = document.getElementById("genderChart").getContext("2d");
			genderChart = new Chart(ctx).Pie(val, settings);
		})
		.fail(function(data) {
		});
	</script>
	<script src="{{asset("/visitStat.js")}}"></script>
	<script type="text/javascript">
		var ctx = document.getElementById("visitChart").getContext("2d");
		var visitChart = new Chart(ctx).Line(visitData, settings);
	</script>
	<script type="text/javascript">
	    var data = [], totalPoints = 100;
	    function getRandomData() {
	      if (data.length > 0)
	        data = data.slice(1);
	      // Do a random walk
	      while (data.length < totalPoints) {
	        var prev = data.length > 0 ? data[data.length - 1] : 50,
	            y = prev + Math.random() * 10 - 5;
	        if (y < 0) {
	          y = 0;
	        } else if (y > 100) {
	          y = 100;
	        }
	        data.push(y);
	      }
	      // Zip the generated y values with the x values
	      var res = [];
	      for (var i = 0; i < data.length; ++i) {
	        res.push([i, data[i]]);
	      }
	      return res;
	    }
	    var interactive_plot = $.plot("#interactive", [getRandomData()], {
	      grid: {
	        borderColor: "#f3f3f3",
	        borderWidth: 1,
	        tickColor: "#f3f3f3"
	      },
	      series: {
	        shadowSize: 0, // Drawing is faster without shadows
	        color: "#3c8dbc"
	      },
	      lines: {
	        fill: true, //Converts the line chart to area chart
	        color: "#3c8dbc"
	      },
	      yaxis: {
	        min: 0,
	        max: 100,
	        show: true
	      },
	      xaxis: {
	        show: true
	      }
	    });

	    var updateInterval = 500; //Fetch data ever x milliseconds
	    var realtime = "on"; //If == to on then fetch data every x seconds. else stop fetching
	    function update() {

	      interactive_plot.setData([getRandomData()]);

	      // Since the axes don't change, we don't need to call plot.setupGrid()
	      interactive_plot.draw();
	      if (realtime === "on")
	        setTimeout(update, updateInterval);
	    }

	    //INITIALIZE REALTIME DATA FETCHING
	    if (realtime === "on") {
	      update();
	    }
	    //REALTIME TOGGLE
	    $("#realtime .btn").click(function () {
	      if ($(this).data("toggle") === "on") {
	        realtime = "on";
	      }
	      else {
	        realtime = "off";
	      }
	      update();
	    });
	    /*
	     * END INTERACTIVE CHART
	     */
	</script>
@endsection


