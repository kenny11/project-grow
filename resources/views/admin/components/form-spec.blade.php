<div class="box box-solid box-primary">
	<div class="box-header">
		<h3 class="box-title">{{$formTitle}}</h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div><!-- /.box-header -->
	<div class="box-body">
		<form class="form-horizontal" id="{{$formID}}" action="{{URL::to('admin/create/user')}}" method="POST">
			<input type="hidden" name="textik" value="false">
			<div class="form-group">
				<label class="col-sm-2 control-label" for="inputName{{$formNumber}}">
					Name
				</label>
				<div class="col-sm-10">
					<input id="inputName{{$formNumber}}" class="form-control" type="text" placeholder="Name" name="name" required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="inputEmail{{$formNumber}}">
					Slug
				</label>
				<div class="col-sm-10">
					<input id="inputEmail{{$formNumber}}" class="form-control" type="email" placeholder="Email" name="email" required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="inputPermissions{{$formNumber}}">
					Permissions
				</label>
				<div class="col-sm-10">
					<select id="inputPermissions{{$formNumber}}" class="form-control" type="text" placeholder="Permissions" name="permissions">
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="inputRoles{{$formNumber}}">
					Roles
				</label>
				<div class="col-sm-10">
					<select id="inputRoles{{$formNumber}}" class="form-control" type="text" placeholder="Roles" name="roles">
					</select>
				</div>
			</div>
			<div class="box-footer">
				<div class="col-sm-7">
					
				</div>
				<div class="col-sm-2">
					<div class="checkbox">
						<label class="col-sm-2 control-label">
							<input type="checkbox" name="edit">
							Edit
						</label>
					</div>
				</div>
				<div class="col-sm-3">
					<button class="btn btn-info pull-right" type="submit">Add</button>
				</div>
			</div>
		</form>
	</div><!-- /.box-body -->
	<div class="overlay" id="{{$formID}}Overlay" style="display: none">
		<i class="fa fa-refresh fa-spin"></i>
	</div>
</div><!-- /.box -->