<div class="box box-solid box-info">
	<div class="box-header">
		<h3 class="box-title">{{$tableTitle}}</h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div><!-- /.box-header -->
	<div class="box-body">
		<table id="{{$tableID}}" class="table table-striped table-bordered special" cellspacing="0" width="100%">
	        <thead>
	            <tr>
	                <th>ID</th>
	                <th>Name</th>
	                <th>Permissions</th>
	                <th>Roles</th>
	                <th>Controlls</th>
	            </tr>
	        </thead>
	        <tfoot>
	            <tr>
	                <th>ID</th>
	                <th>Name</th>
	                <th>Permissions</th>
	                <th>Roles</th>
	                <th>Controlls</th>
	            </tr>
	        </tfoot>
	    </table>
	</div><!-- /.box-body -->
</div><!-- /.box -->