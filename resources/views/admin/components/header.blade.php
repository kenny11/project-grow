<!-- Main Header -->
<header class="main-header">
<?php $user = Auth::user(); ?>
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">{{trans('site/admin.header-mini')}}</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">{{trans('site/admin.header-large')}}</span>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <?php $newMessages = $user->threadsWithNewMessages(); ?>
                        <span class="label label-success">{{count($newMessages)}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">
                            {{trans_choice("site/main.nav-messages", count($newMessages), ["count" => count($newMessages)])}}
                        </li>
                        <li>
                            <div class="slimScrollDiv">
                               <ul class="menu">
                                    @foreach($newMessages as $thread_id)
                                        <?php $thread = Helper::getThread($thread_id) ?>
                                        @include('admin.components.conversation-proto')
                                    @endforeach
                                </ul>
                            </div>
                        </li>
                        <li class="footer">
                            <a href="{{URL::to('messages')}}">{{trans('site/main.nav-messages-showall')}}</a>
                        </li>
                    </ul>
                </li>

                <!-- Notifications Menu -->
                <li class="dropdown notifications-menu">
                    <!-- Menu toggle button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning">10</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 10 notifications</li>
                        <li>
                            <!-- Inner Menu: contains the notifications -->
                            <ul class="menu">
                                <li><!-- start notification -->
                                    <a href="#">
                                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                    </a>
                                </li><!-- end notification -->
                            </ul>
                        </li>
                        <li class="footer"><a href="#">View all</a></li>
                    </ul>
                </li>
                <li class="dropdown user user-menu">
                    <a href="{{URL::to($user->id)}}">
                        <img src="{{asset($user->profileImagePath())}}" class="user-image" alt="User Image"/>
                        <span class="hidden-xs">{{Helper::userFullName($user)}}</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>