<div class="box box-solid box-primary">
	<div class="box-header">
		<h3 class="box-title">{{$formTitle}}</h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div><!-- /.box-header -->
	<div class="box-body">
		<form class="form-horizontal" id="{{$formID}}" action="{{URL::to('admin/create/'.substr($formID, 0, -5))}}" method="POST">
			<input type="hidden" name="textik" value="false">
			<div class="form-group">
				<label class="col-sm-2 control-label" for="inputName{{$formNumber}}">
					Name
				</label>
				<div class="col-sm-10">
					<input id="inputName{{$formNumber}}" class="form-control" type="text" placeholder="Name" name="name" required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="inputSlug{{$formNumber}}">
					Slug
				</label>
				<div class="col-sm-10">
					<input id="inputSlug{{$formNumber}}" class="form-control" type="text" placeholder="Slug (lowercase with dots instead of space)" name="slug" required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="inputDescription{{$formNumber}}">
					Description
				</label>
				<div class="col-sm-10">
					<input id="inputDescription{{$formNumber}}" class="form-control" type="text" placeholder="Description (can be empty)" name="description">
				</div>
			</div>
			<div class="box-footer">
				<div class="col-sm-7">
					
				</div>
				<div class="col-sm-2">
					<div class="checkbox">
						<label class="col-sm-2 control-label">
							<input type="checkbox" name="edit">
							Edit
						</label>
					</div>
				</div>
				<div class="col-sm-3">
					<button class="btn btn-info pull-right" type="submit">Add</button>
				</div>
			</div>
		</form>
	</div><!-- /.box-body -->
	<div class="overlay" id="{{$formID}}Overlay" style="display: none">
		<i class="fa fa-refresh fa-spin"></i>
	</div>
</div><!-- /.box -->