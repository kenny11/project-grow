{
  "data": [
    @foreach ($data as $item)
      [
        "{{$item->id}}",
        "{{$item->name}}",
        "{{$item->slug}}",
        "{{$item->description}}"
      ]@if($last->id!=$item->id),@endif
    @endforeach
  ]
}