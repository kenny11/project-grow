<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Created by Dan Balarin
    </div>
    <!-- Default to the left -->
    <strong>Copyright © 2015 <a href="https://almsaeedstudio.com/">Almsaeed Studio</a>.</strong> All rights reserved.
</footer>