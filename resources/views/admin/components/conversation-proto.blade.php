<li>
    <a href="{{URL::to('m/'.$thread->id)}}">
    	<?php $lastMessage = $thread->getLatestMessageAttribute() ?>
        <div class="pull-left">
            <img src="{{asset(Helper::getUser($lastMessage->user_id)->profileImagePath())}}" class="img-circle" alt="{{Helper::userFullName(Helper::getUser($lastMessage->user_id))}}"/>
        </div>
        <h4>
            {!! Helper::threadName($thread, True); !!}
        </h4>
        <p>{{Helper::getUser($lastMessage->user_id)->first_name}}: {{$lastMessage->body}}</p>
    </a>
</li>