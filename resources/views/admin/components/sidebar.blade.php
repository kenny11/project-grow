<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{trans('site/admin.nav-main')}}</li>
            @permission('admin.dashboard')
                <li class="{{Helper::setActive('admin/dashboard')}}">
                  <a href="{{URL::to('admin/dashboard')}}">
                    <i class="fa fa-dashboard"></i> <span>{{trans('site/admin.nav-dashboard')}}</span>
                  </a>
                </li>
            @endpermission
            @permission('admin.statistics')
                <li class="{{Helper::setActive('admin/statistics')}}">
                  <a href="{{URL::to('admin/statistics')}}">
                    <i class="fa fa-line-chart"></i> <span>{{trans('site/admin.nav-statistics')}}</span>
                  </a>
                </li>
            @endpermission
            @permission('admin.users')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>{{trans('site/admin.nav-users')}}</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                        @permission('admin.users.overview')
                            <li class="{{Helper::setActive('admin/users/overview')}}">
                                <a href="{{URL::to('admin/users/overview')}}">
                                    <i class="fa fa-user"></i>
                                    {{trans('site/admin.nav-users-overview')}}
                                </a>
                            </li>
                        @endpermission
                        @permission('admin.users.permissions')
                            <li class="{{Helper::setActive('admin/users/permissions')}}">
                                <a href="{{URL::to('admin/users/permissions')}}">
                                    <i class="fa fa-lock"></i>
                                    {{trans('site/admin.nav-users-permissions')}}
                                </a>
                            </li>
                        @endpermission
                    </ul>
                </li>
            @endpermission
            @permission('admin.develop')
                <li class="{{Helper::setActive('admin/develop')}}">
                  <a href="{{URL::to('admin/develop')}}">
                    <i class="fa fa-wrench"></i> <span>{{trans('site/admin.nav-develop')}}</span>
                  </a>
                </li>
            @endpermission
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>