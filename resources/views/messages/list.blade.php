@extends('main.components.app')
@section('styles')
@parent
<style type="text/css">
	.user-block-big img
	{
		height: 60px;
		width: 60px;
		float: left;
		margin-right: 10px;
	}
	.conversation-names
	{
		font-size: 1.2em;
	}
	.conversation-names
	{
		margin-left: 6px;
	}
	.unread
	{
		font-weight: bold;
	}
	.conversation
	{
		cursor: pointer;
	}
	.last-message 
	{
		margin-left: 70px;
	}
</style>
@endsection
@section('content')
<?php 
if(Auth::check())
	$user = Auth::user();
?>
<div class="row" style="margin-left: 8%; margin-right: 8%">
	<div class="col-md-3">
		@include('main.components.nav-menu')
	</div>
	<div class="col-md-9">
		@if($threads->count()>0)
			@foreach ($threads as $thread)
			    @include('messages.components.conversation-proto')
			@endforeach
		@else 
		    <div class="container">
		        <div class="alert alert-warning" role="alert">
		            <h4>Warning!</h4>
		            <p>No messages found</p>
		        </div>
		    </div>
		@endif
	</div>
</div>
@endsection
@section('scripts')
@parent
<script type="text/javascript">
	$(document).ready(function(){
		$(".conversation").click(function(e) {
			e.preventDefault;
			window.location.href = "{{URL::to('/m/')}}/" + $(this).data('thread');
		});
	})
</script>
@endsection