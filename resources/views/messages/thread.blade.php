@extends('main.components.app')
@section('styles')
	@parent
	<style type="text/css">
		.direct-chat-img
		{
			border-radius: 4px;
		}
		#sendChatMessage
		{
			float: right;
		}
	</style>
@endsection
@section('content')
<?php 
if(Auth::check())
	$user = Auth::user();
?>
<div class="row" style="margin-left: 8%; margin-right: 8%">
	<div class="col-md-3">
		@include('main.components.nav-menu')
	</div>
	<div class="col-md-9">
		<div class="box box-info direct-chat direct-chat-info box-solid">
		    <div class="box-header with-border">
		        <h3 class="box-title">{!! Helper::threadName($thread) !!}</h3>
		    </div>
		    <div class="box-body">
		        <div class="direct-chat-messages">
					@foreach($thread->messages->reverse()->take(10)->reverse() as $message)
						@include('messages.components.msg-proto')
					@endforeach
		        </div>
			</div>
		    <div class="box-footer">
	        	<form action="{{URL::to('messages')}}" method="post" accept-charset="utf-8" data-ajax="true" id="message">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<?php $arr = array_diff($thread->participantsUserIds()->toArray(), [Auth::id()]); ?>
		            <input type="hidden" name="recipients[]" value="{{implode(',',$arr)}}">

			        <textarea type="text" name="message" placeholder="{{ trans('site/conversations.type-message') }}" class="form-control" style="resize: none;" rows="1"></textarea>
		            <span class="input-group-btn">
		            	<input type="submit" value="{{ trans('site/conversations.send-BTN') }}" 
		            	class="btn btn-info btn-flat" id="sendChatMessage">
		            </span>
		        </form>
		    </div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
@parent
<script type="text/javascript">
	$(document).ready(function() {
		$("#message").submit(function(event) {
			$("#sendChatMessage").prop('disabled', true);
		});
		autosize($('textarea'));
		scrollDown(".direct-chat-messages");
		$.ajaxSetup(
		{
		    headers:
		    {
		        'X-CSRF-Token': $('input[name="_token"]').val()
		    }
		});
		var iteration = 0;
		if(window.socket != null)
		{
			window.socket.on('message', function (id) {
				console.log(id);
				getMessageProto(id);
			})
		}
		function getMessageProto(id)
		{
			$.ajax({
				url: '{{URL::to("api/m/msg-proto")}}',
				type: 'POST',
				data: {'id': id},
			})
			.done(function(data) {
				$("textarea[name='message']").first().val("");
				$(".direct-chat-messages").append(data);
				scrollDown(".direct-chat-messages");
				$("#sendChatMessage").prop('disabled', false);
			})
			.fail(function() {
				if(iteration<10)
					getMessageProto(id);
				else
					$("#sendChatMessage").prop('disabled', false);
				iteration++;
			})
		}
		function scrollDown(selector)
		{
			$(selector).animate({scrollTop: $(selector)[0].scrollHeight});
		}
		var num = 10;
		var offset = num;
		var called = false;
		$(".direct-chat-messages").scroll(function(event) {
			if($(this).scrollTop()==0 && !called)
			{
				called = true;
				var firstMsg = $(".direct-chat-messages").children().first();
				$.ajax({
					url: '{{URL::to("api/m/thread-messages")}}',
					type: 'POST',
					data: {num: num, offset: offset, thread: {{$thread->id}}},
				})
				.done(function(data) {
					$(".direct-chat-messages").prepend(data);
					$(".direct-chat-messages").scrollTop(firstMsg.offset().top-firstMsg.height()*2);
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					called = false;
				});
				offset += num;
			}
		});
	});
</script>
@endsection