@extends('main.components.app')
@section('content')
<?php 
if(Auth::check())
	$user = Auth::user();
?>
<div class="row" style="margin-left: 8%; margin-right: 8%">
	<div class="col-md-3">
		@include('main.components.nav-menu')
	</div>
	<div class="col-md-9">
		<h3>Create a new message</h3>
		<form action="{{URL::to('messages')}}" method="post" accept-charset="utf-8">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="col-md-6">
			    <!-- Subject Form Input -->
			    <div class="form-group">
			    	<input type="text" name="subject" value="" placeholder="subject" class="form-controll">
			    </div>

			    <!-- Message Form Input -->
			    <div class="form-group">
					<textarea name="message" class="form-controll"></textarea>
			    </div>

			    @if($user->friendsCount() > 0)
				    <div class="checkbox">
				        @foreach($user->friends as $user)
				            <label title="{!!$user->name!!}"><input type="checkbox" name="recipients[]" value="{!!$user->id!!}">{!!$user->name!!}</label>
				        @endforeach
				    </div>
			    @endif
			    
			    <!-- Submit Form Input -->
			    <div class="form-group">
			        <input type="submit" name="" value="btn btn-primary form-control">
			    </div>
			</div>
		</form>
	</div>
</div>
@endsection