<?php 
    $mUser = Helper::getUser($message->user_id);
    $author = Helper::isUserSame($message->user_id);
?>
<a data-original-title="{{$message->created_at}}" data-toggle="tooltip" data-placement="top" title="{{$message->created_at}}" href="#" rel="tooltip">
    <div class="direct-chat-msg @if($author) right @endif">
        <div class="direct-chat-info clearfix">
            <span class="direct-chat-name @if($author) pull-right @else pull-left @endif">
                {{$mUser->first_name}}&nbsp;{{$mUser->last_name}}
            </span>
            <span class="direct-chat-timestamp @if($author) pull-left @else pull-right @endif">
                {{Helper::dateToText($message->updated_at)}}
            </span>
        </div>
        <img class="direct-chat-img" src="{{asset($mUser->profileImagePath())}}" alt="{{$mUser->first_name}}&nbsp;{{$mUser->last_name}}">
        <div class="direct-chat-text">
            {{$message->body}}
        </div>
    </div>
</a>
