<div class="box box-primary box-solid conversation" data-thread="{{$thread->id}}">
	<div class="box-body">
		<div class="user-block-big">
			@foreach($thread->participants as $participant)
				@if(!($participant->user_id == Auth::user()->id))
					<img class="img-rounded" src="{{Helper::getUser($participant->user_id)->profileImagePath()}}" alt="
							{{Helper::getUser($participant->user_id)->first_name}}&nbsp;
							{{Helper::getUser($participant->user_id)->last_name}}">
				@endif
			@endforeach
			<div class="conversation-names">
				{!! Helper::threadName($thread); !!}
			</div>
			<div class="last-message @if($thread->isUnread(Auth::user()->id)) unread @endif">
			<?php $lastMessage = $thread->getLatestMessageAttribute() ?>
				@if(Helper::isUserSame($lastMessage->user_id))
					{{ trans('site/conversations.you') }}
				@else
					{{Helper::getUser($lastMessage->user_id)->first_name}}
				@endif
				: {{$lastMessage->body}}
			</div>
		</div>
	</div>
</div>