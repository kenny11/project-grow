@extends('main.components.app')

{{-- Web site Title --}}
{{-- @section('title') {{{ trans('site/user.login') }}} :: @parent @stop --}}

{{-- Content --}}
@section('content')

<div class="container">
    <div class="row">
        <div class="page-header">
            <h2>{{ trans('site/user.login') }}</h2>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            @include('errors.list')

            <form class="form-horizontal" role="form" method="POST" action="{{ URL::to('/auth/login') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label class="col-md-4 control-label">{{ trans('site/user.e-mail') }}</label>

                    <div class="col-md-6">
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">{{ trans('site/user.password') }}</label>

                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember">{{ trans('site/user.remember-me') }}
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary" style="margin-right: 15px;">
                            {{ trans('site/user.BTN-login') }}
                        </button>

                        <a href="{{ URL::to('/password/email') }}">{{ trans('site/user.forgot-password') }}</a>
                        |
                        <a href="{{ URL::to('/auth/register') }}">{{ trans('site/user.not-member') }}</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
