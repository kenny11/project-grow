@extends('main.components.app')

{{-- Web site Title --}}
@section('title') {{{ trans('site/user.register') }}} :: @parent @stop

{{-- Content --}}
@section('content')
<div class="container">
    <div class="row">
        <div class="page-header">
            <h2>{{{ trans('site/user.register') }}}</h2>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            @include('errors.list')

            <form class="form-horizontal" role="form" method="POST" action="{{ URL::to('/auth/register') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label class="col-md-4 control-label">{{ trans('site/user.name') }}</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">{{ trans('site/user.username') }}</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="username"
                               value="{{ old('username') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">{{ trans('site/user.e_mail') }}</label>

                    <div class="col-md-6">
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">{{ trans('site/user.password') }}</label>

                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">{{ trans('site/user.confirm-password') }}</label>

                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password_confirmation">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary" style="margin-right: 15px;">
                            {{ trans('site/user.BTN-register') }}
                        </button>
                        <a href="{{ URL::to('/auth/login') }}">{{ trans('site/user.already-have-account') }}</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
