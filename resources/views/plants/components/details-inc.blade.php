	<div class="col-md-6">
	<h4>{{ trans('site/plant.details-header') }}</h4>
	
		<form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data"
		@if($owner) action="{{ URL::to('/plant/update') }}" @endif>
	
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	    <div class="form-group">
	        <label class="col-md-4 control-label">{{ trans('site/plant.name') }}</label>
	        <div class="col-md-8">
	            <input type="text" class="form-control" name="name" value="{{$plant->name}}" required @if(!$owner) readonly @endif>
	        </div>
	    </div>

	    <div class="form-group">
	        <label class="col-md-4 control-label">{{ trans('site/plant.description') }}</label>
	        <div class="col-md-8">
	        	<textarea name="description" class="form-control" style="resize: vertical;" rows="1.5" required @if(!$owner) readonly @endif>{{$plant->description}}</textarea>
	        </div>
	    </div>
	@if($owner)
		    <div class="form-group">
		        <label class="col-md-4 control-label">{{ trans('site/plant.image') }}</label>
		        <div class="col-md-8">
					<div class="input-group">
						<input class="form-control" type="text" name="imagename" value="{{$plant->image_name}}" readonly>
						<span class="input-group-btn">
							<span class="btn btn-primary btn-file">
								{{ trans('site/plant.browse') }}
								<input type="file" accept="image/png, image/jpeg" name="image" id="image">
							</span>
						</span>
					</div>
		        </div>
		    </div>

		    <div class="form-group">
		        <label class="col-md-4 control-label">{{ trans('site/plant.color') }}</label>
		        <div class="col-md-8 input-group">
		        	<span class="input-group-addon">#</span>
		            <input type="text" class="form-control" name="color" maxlength="6" value="{{$plant->color}}" required>
					<div class="input-group-btn">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></button>
						<ul class="dropdown-menu dropdown-menu-right">
							<li><a href="#48CFAD">{{ trans('site/plant.color-mint') }}</a></li>
							<li><a href="#FC6E51">{{ trans('site/plant.color-bittersweet') }}</a></li>
							<li><a href="#FFCE54">{{ trans('site/plant.color-sunflower') }}</a></li>
							<li><a href="#EC87C0">{{ trans('site/plant.color-pink-rose') }}</a></li>
							<li><a href="#4FC1E9">{{ trans('site/plant.color-aqua') }}</a></li>
						</ul>
					</div>
		        </div>
		    </div>

		    <div class="form-group">
		        <div class="col-md-6 col-md-offset-3">
		            <button type="submit" class="btn btn-primary" style="margin-right: 15px;">
		                {{ trans('site/plant.update-BTN') }}
		            </button>
		            <input type="hidden" name="plant_id" value="{{$plant->id}}">
		        </div>

		    </div>

	@endif
	</form>

	</div>
	<div class="col-md-6">
		<h4>{{ trans('site/plant.preview') }}</h4>
		@include('plants.components.plant-proto')	
	</div>
	@if($owner)
		<span style="color: #AAB2BD;">{{ trans('site/plant.recomended-image-size') }}: 200x125px</span>
	@endif
</div>