<div class="col-md-6">
	<h4>{{ trans('site/plant.add-header') }}</h4>
		<form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ URL::to('/plant/add') }}">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	    <div class="form-group">
	        <label class="col-md-4 control-label">{{ trans('site/plant.name') }}</label>
	        <div class="col-md-6">
	            <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Plant" required>
	        </div>
	    </div>

	    <div class="form-group">
	        <label class="col-md-4 control-label">{{ trans('site/plant.description') }}</label>
	        <div class="col-md-6">
	        	<textarea name="description" class="form-control" placeholder="Description" style="resize: vertical;" rows="1.5" required></textarea>
	        </div>
	    </div>

	    <div class="form-group">
	        <label class="col-md-4 control-label">{{ trans('site/plant.image') }}</label>
	        <div class="col-md-6">
				<div class="input-group">
					<input class="form-control" type="text" name="imagename" readonly>
					<span class="input-group-btn">
						<span class="btn btn-primary btn-file">
							{{ trans('site/plant.browse') }}
							<input type="file" accept="image/png, image/jpeg" name="image" id="image">
						</span>
					</span>
				</div>
	        </div>
	    </div>

	    <div class="form-group">
	        <label class="col-md-4 control-label">{{ trans('site/plant.color') }}</label>
	        <div class="col-md-6 input-group">
	        	<span class="input-group-addon">#</span>
	            <input type="text" class="form-control" name="color" maxlength="6" value="FC6E51" required>
				<div class="input-group-btn">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></button>
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#48CFAD">{{ trans('site/plant.color-mint') }}</a></li>
						<li><a href="#FC6E51">{{ trans('site/plant.color-bittersweet') }}</a></li>
						<li><a href="#FFCE54">{{ trans('site/plant.color-sunflower') }}</a></li>
						<li><a href="#EC87C0">{{ trans('site/plant.color-pink-rose') }}</a></li>
						<li><a href="#4FC1E9">{{ trans('site/plant.color-aqua') }}</a></li>
					</ul>
				</div>
	        </div>
	    </div>

	    <div class="form-group">
	        <div class="col-md-6 col-md-offset-4">
	            <button type="submit" class="btn btn-primary" style="margin-right: 15px;">
	                {{ trans('site/plant.create-BTN') }}
	            </button>
	        </div>
	    </div>
	</form>
	</div>
	<div class="col-md-6">
		<h4>{{ trans('site/plant.preview') }}</h4>
		<div class="plant" style="background-color: #FC6E51; color: #F5F7FA;">
		    <img src="http://www.western-chilli.cz/images/papricka.jpg" alt="Chilli">
		    <div class="name">
		        {{ trans('site/plant.add-plant') }}
		    </div>
		</div>
	</div>
	<span style="color: #AAB2BD;">{{ trans('site/plant.recomended-image-size') }}: 200x125px</span>