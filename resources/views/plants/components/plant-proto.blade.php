<a href="{{URL::to('plant/'.$plant->id)}}" title="{{$plant->name}}" style="display: table; margin: auto">
    <div class="col-md-3 plant" style="padding-left: 7px; padding-right: 7px;">
        <div class="panel panel-plant">
            <div class="panel-heading" style="background-color: #{{$plant->color}};border-color: #{{$plant->color}}">
                <h3 class="panel-title plant-name">{{$plant->name}}</h3>
            </div>
            <div class="panel-body" style="padding: 0px;">
                <img src="{{asset($plant->imagePath())}}" alt="{{$plant->name}}" style="max-width: 100%">
            </div>
            <div class="panel-footer plant-description">
                {{$plant->description}}
                @if(Helper::isUserSame($plant->user_id))
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary btn-xs share" data-plant="{{$plant->id}}" data-href="{{URL::to('post/plant')}}" data-toggle="modal" data-target="#confirm-share">
                            <i class="fa fa-share"></i>
                        </button>
                    </div>
                @endif
            </div>
        </div>
    </div>
</a>