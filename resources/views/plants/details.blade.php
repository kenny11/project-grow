@extends('main.components.app')
@section('title')
Chilli - {{$plant->name}}
@endsection
@section('styles')
@parent
<style type="text/css" media="screen">
	.input-group[class*="col-"]{
		padding-left: 15px;
		padding-right: 15px;
	}
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
</style>
@endsection
@section('content')
    <?php 
        if(Auth::check())
            $user = Auth::user();
        else
            $user = "";
    ?>
	<div class="row" style="margin-left: 8%; margin-right: 8%">
		<div class="col-md-3">
			@include('main.components.nav-menu')
		</div>
		<div class="col-md-9">
			@include('plants.components.details-inc')
		</div>
	</div>
@endsection
@if($owner)
	@section('scripts')
	@parent
		<script type="text/javascript">
		$(document).ready(function(){
			function ValidColor(color){
				return /([0-9A-F]{6}$)|([0-9A-F]{3}$)/i.test(color);
			}
			function PreviewReload(){
				if($("input[name='name']").val()!="")
					$(".plant-name").text($("input[name='name']").val());
				if($("input[name='color']").val()!="")
					if(ValidColor($("input[name='color']").val()) === true)
					{
						$(".plant-name").first().parent().css("background-color", '#'+$("input[name='color']").val());
						$(".plant-name").first().parent().css("border-color", '#'+$("input[name='color']").val());
					}
				if($("textarea[name='description']").val()!="")
					$(".plant-description").text($("textarea[name='description']").val());
			};
			$("form ul.dropdown-menu li a").each(function(index){
				$(this).css("color", $(this).context.hash);
			})
			$("form ul.dropdown-menu li a").click(function(event){
				var temp = event.target.hash;
				$("input[name='color']").val(temp.replace('#', ''));
				event.preventDefault();
				PreviewReload();
			})
			$("input, textarea").keyup(function(event) {
				PreviewReload();
			});
			$(".btn-file :file").change(function(){
				var file = $(this)[0].files[0]
				var reader = new FileReader();
			    var image  = new Image();

			    reader.readAsDataURL(file);  
			    reader.onload = function(_file) {
			        image.src    = _file.target.result;            
			        image.onload = function() {
		    			var max_width = 200,
							max_height= 125;
			            var w = this.width,
			                h = this.height,
			                t = file.type,
			                n = file.name,
			                s = ~~(file.size/1024) +'KB';
			            if(w>max_width || h>max_height)
			            	console.log(w+'x'+h+' '+s+' '+t+' '+n);
			            $("input[name='imagename']").val(n);
			            $('.plant img').attr('src', image.src);
			        };
			        image.onerror= function() {
			            alert('Invalid file type: '+ file.type);
			        };      
			    };
			})
		})
		</script>
	@endsection
@endif