@extends('main.components.app')
@section('styles')
@parent
<style type="text/css">
    .plant
    {
        cursor: pointer;
    }
</style>
@endsection
@section('content')
    <?php 
        if(Auth::check())
            $user = Auth::user();
        else
            $user = "";
    ?>
    <div class="modal fade bs-example-modal-sm" id="confirm-share" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <form action="{{URL::to('post/plant')}}" method="POST" accept-charset="utf-8">
                    <div class="modal-header">
                        <h4 class="modal-title" id="gridSystemModalLabel">
                            {{ trans('site/modal.share_header') }}
                        </h4>
                    </div>
                    <div class="modal-body">
                        {{ trans('site/modal.share_plant') }}
                            <input type="hidden" name="plantId" value="null">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <textarea class="form-control autosize_textarea" rows="2" placeholder="{{ trans('site/post.create-post-placeholder') }}" name="message" required></textarea>
                       
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            {{ trans('site/modal.close') }}
                        </button>
                        <input type="submit" class="btn btn-success btn-ok" value="{{ trans('site/modal.share') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row" style="margin-left: 8%; margin-right: 8%">
        <div class="col-md-3">
            @include('main.components.nav-menu')
        </div>
        <div class="col-md-9">
            @if($user->plants()->get()->count()!=0)
                @foreach ($user->plants()->get() as $plant)
                    @include('plants.components.plant-proto')
                @endforeach
            @else 
                <div class="container">
                    <div class="alert alert-warning" role="alert">
                        <h4>Warning!</h4>
                        <p>No plants found, lets start by adding one</p>
                        <p>
                            <a class="btn btn-warning" href="{{URL::to('plant/add')}}">
                                <span class="glyphicon glyphicon-plus"></span> add one
                            </a>
                        </p>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
@section('scripts')
@parent
<script type="text/javascript">
    $(document).ready(function(){

        $(".share").click(function(e) {
            e.preventDefault();
            var plantId = $(this).data('plant');
            $("input[name='plantId']").val($(this).data('plant'));
        });
    })
</script>
@endsection