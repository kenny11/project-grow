@extends('main.components.app')
@section('content')
<?php 
if(Auth::check())
	$user = Auth::user();
?>
<style type="text/css">
	hr{
		border-color: #bbb -moz-use-text-color -moz-use-text-color;
	}
</style>
<div class="row" style="margin-left: 8%; margin-right: 8%">
	<div class="col-md-3">
		@include('main.components.nav-menu')
	</div>
	<div class="col-md-6">
		@include('main.components.feed-inc')
	</div>
	<div class="col-md-3">
		@include('main.components.user-info')
	</div>
</div>
@endsection