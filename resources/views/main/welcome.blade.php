@extends('main.components.app')
@section('content')
<style type="text/css">
	.form-horizontal{
		padding: 10px;
	}
	td{
		border: 4px solid transparent;
	}
	input[type="checkbox"]{
		margin: 4px;
		margin-right: 8px;
	}
	.panel-gray{
		background-color: #e6e9ed;
		border: none;
	}
	.panel-gray .panel-heading{
		background-color: #ccd1d9;
		border-radius: 0px;
		font-size: 1.1em;
	}
</style>

<div class="row" style="margin-top: 30px">
	<div class="col-md-offset-2 col-md-5">
		<h1>
			{{trans('site/welcome.header')}}
		</h1>
	</div>
	<div class="col-md-3">
		<div class="panel panel-gray">
			<div class="panel-heading">
				{{trans('site/welcome.login')}}
			</div>
			<div class="panel-body">
		        <div class="row">
		            @include('errors.list')
		            <form class="form-horizontal" role="form" method="POST" action="{{ URL::to('/auth/login') }}">
		            	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		            	<table>
		            		<tbody>
		            			<tr>
		            				<td colspan="2">
		            					<input type="email" class="form-control" name="email" placeholder="{{ trans('site/user.e-mail') }}" value="{{ old('email') }}">
		            				</td>
		            			</tr>
		            			<tr>
		            				<td>
		            					<input type="password" class="form-control"  placeholder="{{ trans('site/user.password') }}" name="password">
		            				</td>
		            				<td>
		            					<button type="submit" class="btn btn-primary" style="float: right;">
		            					{{ trans('site/user.BTN-login') }}
                        				</button>
		            				</td>
		            			</tr>
		            			<tr>
		            				<td colspan="2">
		            					<input type="checkbox" name="remember">{{ trans('site/user.remember-me') }}
		            				</td>
		            			</tr>
		            			<tr>
									<td colspan="2">
										<a href="{{ URL::to('/password/email') }}">
											{{ trans('site/user.forgot-password') }}
										</a>
									</td>
		            			</tr>
		            		</tbody>
		            	</table>
		            </form>
		        </div>
			</div>
		</div>
		<div class="panel panel-gray" style="margin-top: 20px;">
			<div class="panel-heading">
				{{trans('site/welcome.register')}}
			</div>
			<div class="panel-body">
		        <div class="row">
		            @include('errors.list')
		            <form class="form-horizontal" role="form" method="POST" action="{{ URL::to('/auth/register') }}">
		            	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		            	<table style="width: 100%;">
		            		<tbody>
		            			<tr>
		            				<td>
		            					<input type="text" class="form-control" name="username" placeholder="{{ trans('site/user.username') }}" value="{{ old('username') }}">
                               		</td>
		            			</tr>
		            			<tr>
		            				<td>
		            					<input type="email" class="form-control" name="email" placeholder="{{ trans('site/user.e-mail') }}" value="{{ old('email') }}">
		            				</td>
		            			</tr>
		            			<tr>
		            				<td>
		            					<input type="password" class="form-control"  placeholder="{{ trans('site/user.password') }}" name="password">
		            				</td>
		            			</tr>
		            			<tr>
		            				<td>
		            					 <input type="password" class="form-control" placeholder="{{ trans('site/user.confirm-password') }}" name="password_confirmation">
		            				</td>
		            			</tr>
		            			<tr>
		            				<td>
		            					<button type="submit" class="btn btn-primary" style="float: right;">
		            						{{ trans('site/user.BTN-register') }}
                        				</button>
		            				</td>
		            			</tr>
		            		</tbody>
		            	</table>
		            </form>
		        </div>
			</div>
		</div>
	</div>
	<div class="col-md-offset-2">
		
	</div>	
</div>


@endsection