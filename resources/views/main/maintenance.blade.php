@extends('main.components.app')
@section('content')
<style type="text/css">
	.maintenanceContent{
		width: 30em;
		height: 9em;
		background-color: #A0D468;
		margin: auto;
		margin-top: 30px;
		color: white;
		padding: 0.5em;
		display:table;
	}
	.maintenanceContent i{
		font-size: 8em;
		float: left;
		display:table-cell;
 		vertical-align:middle;
	}
	.maintenanceContent > span{
		display:table-cell;
 		vertical-align:middle;
	}
	.main_text{
		font-size: 1.6em;
	}
	.try_again{
		font-size: 1.2em;
	}
</style>
<div class="maintenanceContent">
	<i class="fa fa-wrench"></i>
	<span>
		<span class="main_text">{{ trans('site/maintenance.main_text') }}</span>
		<br>
		<span class="try_again">{{ trans('site/maintenance.try_again') }}</span>
	</span>
</div>
@endsection