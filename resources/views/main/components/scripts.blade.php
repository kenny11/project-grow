    <script src="{{asset('jquery.js')}}"></script>
    <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('bootflat/js/icheck.min.js')}}"></script>
    <script src="{{asset('bootflat/js/jquery.fs.selecter.min.js')}}"></script>
    <script src="{{asset('bootflat/js/jquery.fs.stepper.min.js')}}"></script>
    <script src="{{asset('posts.js')}}"></script>
    <script src="{{asset('ajaxSend.js')}}"></script>
    <script src="{{asset('autosize.js')}}" type="text/javascript" ></script> 
    <script type="text/javascript">
		$(document).ready(function(){
			$('[rel=tooltip]').tooltip();
		})
	</script>
    @if(Auth::check())
        <script src="{{asset('js/socket.io.js')}}"></script>
        <script type="text/javascript">
        $(document).ready(function() {
            window.socket = io.connect('http://{{config("servers.socketServerAddress")}}',{
                query: "token={{Auth::user()->token}}",
                'reconnection limit': 1000*60/2,
                'max reconnection attempts' : 'Infinity'
            });
            window.socket.on('connect',function() {
                console.log('Client has connected to the {{config("servers.socketServerName")}} server!');
            });
        });
        </script>
    @endif