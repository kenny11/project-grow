
@if(Helper::isDownForMaintenance() && Auth::check() && Auth::user()->can('view.maintenance'))
	<style type="text/css">
		.maintenance
		{
			display: block;
			font-size: 1em;
		}
	</style>
	<span class="label label-danger maintenance">{{ trans('site/maintenance.header') }}</span>
@endif
<style type="text/css">
	input[type="checkbox"]{
		margin: 4px;
		margin-right: 8px;
	}
</style>



<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header" style="margin-left: 10%;">
      		<a class="navbar-brand" href="{{ URL::to('/') }}">{{config('site.name')}}</a>
        </div>
		<div class="collapse navbar-collapse">
	        <ul class="nav navbar-nav">
	        </ul>
	        <ul class="nav navbar-nav navbar-right">
	        	@if(Auth::check() && !Helper::isDownForMaintenance())
                    <li class="dropdown messages-menu">
	                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	                        <i class="fa fa-envelope-o"></i>
	                        <?php $newMessages = $user->threadsWithNewMessages(); ?>
	                        <span class="label label-success upper-index">{{count($newMessages)}}</span>
	                    </a>
	                    <ul class="dropdown-menu">
	                        <li class="header">
	                            {{trans_choice("site/main.nav-messages", count($newMessages), ["count" => count($newMessages)])}}
	                        </li>
	                        <li>
	                            <div class="slimScrollDiv">
	                               <ul class="menu">
	                                    @foreach($newMessages as $thread_id)
	                                        <?php $thread = Helper::getThread($thread_id) ?>
	                                        @include('admin.components.conversation-proto')
	                                    @endforeach
	                                </ul>
	                            </div>
	                        </li>
	                        <li class="footer">
	                            <a href="{{URL::to('messages')}}">{{trans('site/main.nav-messages-showall')}}</a>
	                        </li>
	                    </ul>
	                </li><!--
		        	<li class="dropdown notifications-menu">
		                <a aria-expanded="false" href="#" class="dropdown-toggle" data-toggle="dropdown">
		                    <i class="fa fa-bell-o"></i>
		                    <span class="label label-warning upper-index">10</span>
		                </a>
		                <ul class="dropdown-menu">
		                    <li class="header">You have 10 notifications</li>
		                    <li>
		                        <ul class="menu">
		                            <li>
		                                <a href="#">
		                                    <i class="fa fa-users text-aqua"></i> 5 new members joined today
		                                </a>
		                            </li>
		                        </ul>
		                    </li>
		                    <li class="footer"><a href="#">View all</a></li>
		                </ul>
		            </li>-->
		        @else

		        @endif
	        </ul>
	    </div>
    </div>
</nav>


