<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('/') }}">Chilli</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                @permission('plant.list')
                <li><a href="{{ URL::to('/plant/list') }}">List</a></li>
                @endpermission
                @permission('admin.dashboard')
                <li><a href="{{ URL::to('/admin/dashboard') }}">Admin</a></li>
                @endpermission
            </ul>
            <ul class="nav navbar-nav navbar-right">
            @if(!Auth::check())
                <li>
                    <a href="{{ URL::to('/auth/login') }}" title="Login">Not logged in</a>
                </li>
            @else
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Signed in as {{Auth::user()->name}} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/{{Auth::user()->id}}">Settings</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ URL::to('/auth/logout') }}">Log out</a></li>
                    </ul>
                </li>

            @endif
            </ul>
        </div>
    </div>
</nav>