<ul class="list-inline">
	<li></li>
	<li class="pull-right"><a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> {{ trans('site/post.comments') }} ({{$post->Comments()->count()}})</a></li>
</ul>