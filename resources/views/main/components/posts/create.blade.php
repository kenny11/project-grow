<style type="text/css">
  .send-post-btn
  {
    float: right;
  }
  .panel{
    background-color: white;
    border: none;
  }
  .nav-tabs li a:hover{
    background-color: #ddd;
  }
  .nav-tabs li.active{
    font-weight: 700;
  }
</style>
<div class="panel create-post">
  <ul id="post-tabs" class="nav nav-tabs nav-justified">
    <li class="active"><a href="#post" data-toggle="tab">
        <i class="fa fa-pencil-square-o"></i>&nbsp;
		{{ trans('site/post.share-text') }}
	</a></li>

    <li><a href="#image" data-toggle="tab">
    	<i class="fa fa-file-image-o"></i>&nbsp;
    	{{ trans('site/post.share-image') }}
    </a></li>
    @permission('admin.develop')
    <li><a href="#album" data-toggle="tab">
    	<i class="fa fa-files-o"></i>&nbsp;
    	{{ trans('site/post.create-album') }}
    </a></li>
    @endpermission
  </ul>
  <div id="post-contents" class="tab-content">
    <div class="tab-pane fade active in" id="post">
    <form action="{{ URL::to('/post/create') }}" method="get" accept-charset="utf-8">
      <textarea class="form-control autosize_textarea" rows="2" placeholder="{{ trans('site/post.create-post-placeholder') }}" name="message" required></textarea>
      <input type="submit" value="{{ trans('site/post.BTN-send-post') }}" class="btn btn-primary btn-flat send-post-btn">
    </form>
      
    </div>
    <div class="tab-pane fade" id="image">
      <form action="{{ URL::to('/post/image') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data" data-ajax="true">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <textarea class="form-control autosize_textarea" rows="2" placeholder="{{ trans('site/post.create-imagepost-placeholder') }}" name="message" required></textarea>
        <input type="file" name="image" accept="image/jpg" name="image" required>
        <input type="submit" value="{{ trans('site/post.BTN-send-image') }}" class="btn btn-primary btn-flat send-post-btn">
      </form>
    </div>
    <div class="tab-pane fade" id="album">
      <p></p>
    </div>
  </div>
</div>