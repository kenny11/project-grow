<?php $plant = Helper::getPlant($post->data) ?>
<div class="post">
	@include('main.components.posts.author')
	<p>
		{{$post->message}}
	</p>
	@include('plants.components.plant-proto')
	@include('main.components.posts.bottom-bar')
	<hr>
	@include('main.components.posts.comments')
</div>