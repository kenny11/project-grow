<style type="text/css">
	.media img
	{
		width: 40px;
		height: 40px;
	}
	.media .user-block
	{
		margin-bottom: 0px;
	}
	.user-block .description
	{
		color: #666;
	}
	.media-list
	{
		margin-top: 10px;
	}
</style>
<div class="post">
	@include('main.components.posts.author')
	<p>
		{{$post->message}}
	</p>
	@include('main.components.posts.bottom-bar')
	<hr>
	@include('main.components.posts.comments')
	
</div>