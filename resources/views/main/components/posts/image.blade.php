<div class="post">
	@include('main.components.posts.author')
	<p>
		{{$post->message}}
	</p>
	<img class="post-img" src="{{ URL::to($post->data) }}" alt="{{$post->message}}" title="{{$post->message}}">
	@include('main.components.posts.bottom-bar')
	<hr>
	@include('main.components.posts.comments')
</div>