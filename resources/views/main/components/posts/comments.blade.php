<style type="text/css">
	.media{
		margin: 0px 10px 0px 10px;
	}
</style>
<form action="{{ URL::to('post/'.$post->id.'/comment/create') }}" method="get" accept-charset="utf-8">
	<input class="form-control input-sm comment" placeholder="{{ trans('site/post.type-comment') }}" type="text" name="message">
</form>
@if($post->Comments()->count() > 0)
	<ul class="media-list">	
	@foreach($post->Comments()->get() as $comment)
		<li class="media">
			<div class="user-block">
				<div class="img-rounded">
					<img class="img-rounded img-bordered-sm" src="{{asset($comment->User()->first()->profileImagePath())}}" alt="user image">
				</div>
				<span class="username">
					<a href="{{URL::to($comment->user_id)}}">
						{{$comment->User()->get()[0]->first_name}}&nbsp;
						{{$comment->User()->get()[0]->last_name}}
					</a>
					@if($comment->isOwner())
						<a href="#" class="pull-right btn-box-tool comment-control-offset"><i class="fa fa-times"></i></a>
						<a href="#" class="pull-right btn-box-tool "><i class="fa fa-pencil"></i></a>
					@endif
				</span>
				<span class="description">
					{{$comment->message}}
				</span>
				<span class="description">
					{{Helper::dateToText($comment->created_at)}} 
					@if($comment->created_at != $comment->updated_at)
					 - ({{ trans('site/post.edited') }})
					@endif
				</span>
			</div>
			<div class="media-body">
			</div>
		</li>
	@endforeach
	</ul>
@endif