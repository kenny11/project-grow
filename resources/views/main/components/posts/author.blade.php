<div class="user-block">
	<div class="img-rounded">
		<img class="img-rounded" src="{{asset($post->User()->first()->profileImagePath())}}" alt="
			{{$post->User()->first()->first_name}}&nbsp;
			{{$post->User()->first()->last_name}}">
	</div>
	<span class="username">
		<a href="{{URL::to($post->user_id)}}">
			{{$post->User()->first()->first_name}}&nbsp;
			{{$post->User()->first()->last_name}}
		</a>

		@if($post->isOwner())
			<a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
			<a href="#" class="pull-right btn-box-tool"><i class="fa fa-pencil"></i></a>
		@endif
	</span>
	<span class="description">
	{{ trans('site/post.shared-publicly') }} - 
	<a data-original-title="{{$post->created_at}}" data-toggle="tooltip" data-placement="top" title="{{$post->created_at}}" href="#" rel="tooltip">
		{{Helper::dateToText($post->created_at)}}
	</a>
	@if($post->created_at != $post->updated_at)
	 - ({{ trans('site/post.edited') }})
	@endif
	</span>
</div>