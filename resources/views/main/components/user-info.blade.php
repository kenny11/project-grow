<div class="box box-primary box-solid">
	<div class="box-body box-profile">
		<a href="{{URL::to($user->id)}}" title="{{ trans('site/right-nav.profile') }}">
			<img class="profile-user-img img-responsive img-rounded" src="{{asset($user->profileImagePath())}}" alt="User profile picture">
			<h3 class="profile-username text-center">
				@if(Auth::check())
					{{Helper::userFullName($user)}}
				@endif
			</h3>
		</a>
		<p class="text-muted text-center">
			@if(Auth::check())
				{{$user->job}}
			@else
				{{ trans('site/right-nav.not_logged_in') }}
			@endif
		</p>
		@if(Auth::check())
			<ul class="list-group list-group-unbordered">
				<li class="list-group-item">
					<a href="{{$user->id}}/friends" title="">
						<b>{{ trans('site/right-nav.friends') }}</b>
						<span class="pull-right">{{$user->friendsCount()}}</span>
					</a>
				</li>
			</ul>
			<!--
			<a href="#" class="btn btn-primary btn-block"><b>{{ trans('site/right-nav.add_as_friend') }}</b>
			-->
			</a>
		@endif
	</div>   
</div>
@if(Auth::check())
	<div class="box box-success box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">{{ trans('site/right-nav.about_me') }}</h3>
		</div><!-- /.box-header -->
		<div class="box-body">
			@if($user->skills!="")
				<strong><i class="fa fa-map-marker margin-r-5"></i> {{ trans('site/right-nav.location') }}</strong>
				<p class="text-muted">{{$user->location}}</p>
			@endif
			@if($user->skills!="")
				<hr>
				<strong><i class="fa fa-pencil margin-r-5"></i> {{ trans('site/right-nav.skills') }}</strong>
				<p>
					@foreach (explode(',',$user->skills) as $skill)
						<span class="label label-success">
						{{$skill}}
						</span>&nbsp;
					@endforeach
				</p>
			@endif
			@if($user->notes!="")
				<hr>
				<strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
				<p>{{$user->notes}}</p>
			@endif
		</div>
	</div>
@endif