<div class="box box-primary box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">{{ trans('site/nav.navigation') }}</h3>
	</div>
	<div class="box-body">
		<div class="list-group list-nav">
			<a href="{{URL::to('/')}}" class="list-group-item {{Helper::setActive('/')}}">
				<i class="fa fa-newspaper-o"></i>
				<span class="label label-normal upper-index">0</span>
				{{ trans('site/nav.feed') }}
			</a>
			@if(Auth::check())
			<a href="{{URL::to('messages')}}" class="list-group-item {{Helper::setActive('messages')}}">
				<i class="fa fa-envelope-o"></i>
				<span class="label label-success upper-index">{{count($user->threadsWithNewMessages())}}</span>
				{{ trans('site/nav.messages') }}
			</a>
			<a href="{{URL::to('/friends')}}" class="list-group-item {{Helper::setActive('friends')}}">
				<i class="fa fa-users"></i>
				@if($user->friendRequestCount()>0)
					<span class="label label-info upper-index">{{$user->friendRequestCount()}}</span>
				@endif
				{{ trans('site/nav.friends') }}
			</a>
			@endif
		</div>
	</div>
	<div class="box-header with-border">
		<h3 class="box-title">{{ trans('site/nav.plants') }}</h3>
	</div><!-- /.box-header -->
	<div class="box-body">
		<div class="list-group list-nav">
			<a href="{{URL::to('plant/add')}}" class="list-group-item {{Helper::setActive('plant/add')}}">
				{{ trans('site/nav.plant-add') }}
			</a>
			<a href="{{URL::to('plant/list')}}" class="list-group-item {{Helper::setActive('plant/list')}}">
				{{ trans('site/nav.plant-list') }}
			</a>
		</div>
	</div>
</div>
@permission('admin.develop')
	<div class="box box-warning box-solid">
		<div class="box-header with-border">
			<i class="fa fa-exclamation"></i>
			Admin
			<i class="fa fa-exclamation"></i>
		</div>
		<div class="box-body">
			<div class="list-group list-nav">
				<a href="{{URL::to('admin')}}" class="list-group-item">
					{{ trans('site/nav.administration') }}
				</a>
			</div>
			@if($user->friendsCount() != 0)
				<strong>Friends</strong>
				<ul>
					@foreach ($user->friends as $friend)
						<li>{{$friend->first_name}}&nbsp;{{$friend->last_name}}</li>
					@endforeach
				</ul>
			@endif
		<strong>Posts</strong>
			<ul>
				@foreach ($user->RelatedPosts() as $post)
					<li>{{$post->message}}</li>
				@endforeach
			</ul>
		</div>
	</div>		
@endpermission
