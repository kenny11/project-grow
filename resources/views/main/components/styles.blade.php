    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('bootflat/css/bootflat.css')}}">
    <link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/dist/css/AdminLTE.css")}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('font-awesome/css/font-awesome.min.css')}}">
    <link rel="shortcut icon" href="{{{ asset('assets/site/ico/favicon.ico') }}}">
    <link rel="stylesheet" href="{{asset('style.css')}}">
    <link rel="stylesheet" href="{{asset('plants.css')}}">
    <link rel="stylesheet" href="{{asset('overrides.css')}}">
    <style type="text/css">
		.autosize_textarea{
			resize:none;
		}
		.list-nav{
			border-radius: none;
			box-shadow: none;
			margin: 0px;
		}
		.list-nav > .list-group-item:first-child{
			border-radius: 0px;
		}
		.list-nav > .list-group-item:last-child{
			border-radius: 0px;
		}
		.list-nav > .list-group-item{
			padding: 5px 10px;
		}
		.upper-index{
			position: relative;
			top: -10px;
			right: 5px;
			text-align: center;
			font-size: 9px;
			padding: 0px 3px;
			line-height: 0.9;
		}
	</style>
	<style type="text/css">
	.media img
	{
		width: 40px;
		height: 40px;
	}
	.media .user-block
	{
		margin-bottom: 0px;
	}
	.user-block .description
	{
		color: #666;
	}
	.media-list
	{
		margin-top: 10px;
	}
	.post-img
	{
		max-width: 100%;
		max-height: 300px;
	    display: block;
	    margin-left: auto;
	    margin-right: auto;
	    margin-bottom: 10px;
	}
	.user-proto-image{
		height: 100px;
		width: 100px;
		float: left;
		margin-right: 10px;
	}
	.user-proto-image img{
		height: 100%;
		width: auto;
	}
	.user-proto-name{
		word-wrap: normal;
		text-align: center;
		vertical-align: middle;
		height: 100%;
		display: table; 
	}
	.user-proto-name span{
		display: table-cell;
		vertical-align: middle;
		text-align: center;
		font-size: 14px;
	}
	.user-proto-controll{
		position: absolute;
		bottom: 15px;
		right: 20px;
	}
	.user-proto-controll .btn{
		padding: 0px 4px;
		float: right;
	}
	.user-proto-box{
		height: 100px;
		background-color: #E6E9ED;
		margin-bottom: 10px;
	}
</style>