@permission('post.create')
<style type="text/css">
	.create-post{
		box-shadow:none;
		margin-bottom: 0px;
	}
</style>
	<div class="box box-primary box-solid">
		<div class="box-body">
			@include('main.components.posts.create')
		</div>
	</div>
@endpermission
@if(Auth::check() && Auth::user()->Posts()->count()!=0)
	{{--@foreach (Auth::user()->Posts()->orderBy('created_at', 'DESC')->get() as $post)--}}
	@foreach ($user->RelatedPosts() as $post)
		<div class="box box-primary box-solid">
			<div class="box-body">
				@include('main.components.posts.'.$post['type'])
			</div>
		</div>
	@endforeach
@else
	<div class="box box-primary box-solid">
		<div class="box-body">
			<h5>{{ trans('site/post.nothing-to-show') }}</h5>
		</div>
	</div>
@endif

