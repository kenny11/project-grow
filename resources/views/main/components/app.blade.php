<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@section('title') {{config('site.name')}} @show</title>
    @section('meta_keywords')
        <meta name="keywords" content="your, awesome, keywords, here"/>
    @show 
    @section('meta_author')
        <meta name="author" content="Dan Balarin"/>
    @show 
    @section('meta_description')
        <meta name="description" content=""/>
    @show
    @section('meta_og')

    @show
    @section('styles')
        @include('main.components.styles')
    @show
</head>
<body style="background-color: #ddd">
    <!--@include('main.components.nav')-->
    @section('scripts')
        @include('main.components.scripts')
    @show
    @include('main.components.nav2')
    @yield('content')
    
</body>
</html>