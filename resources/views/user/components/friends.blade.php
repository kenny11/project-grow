<div class="modal fade bs-example-modal-sm" id="confirm-delete" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="gridSystemModalLabel">
					{{ trans('site/modal.delete_header') }}
				</h4>
			</div>
			<div class="modal-body">
				{{ trans('site/modal.delete_main') }}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">
					{{ trans('site/modal.close') }}
				</button>
                <a class="btn btn-danger btn-ok" data-ajax="true">
					{{ trans('site/modal.delete') }}
                </a>
			</div>
		</div>
	</div>
</div>
<div class="box box-primary box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">{{ trans('site/nav.friends') }}</h3>
	</div>
	<div class="box-body">
		@if($user->friendRequestCount())
			<div class="list-group list-nav" style="border-bottom: 1px solid #D2D6DE;">
				<div class="row">
					<?php $friendRequest = True; ?>

					@foreach ($user->friendRequests()->get() as $friend)
						<div class="col-md-6">
							@include('user.components.friend-proto')
						</div>
					@endforeach
					<?php unset($friendRequest) ?>
				</div>
			</div>
		@endif
		@if($user->friendsCount())
			<div class="list-group list-nav">
				<div class="row" id="friends">
					@foreach ($user->friends as $friend)
						<div class="col-md-6" data-user="{{$friend->id}}">
							@include('user.components.friend-proto')
						</div>
					@endforeach
				</div>
			</div>
		@endif
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#confirm-delete').on('show.bs.modal', function(e) {
			//console.log($(this).find('.btn-ok').data('action', $(e.relatedTarget).data('action')));
		    $(this).find('.btn-ok')
		    	.data('action', $(e.relatedTarget).data('action'))
		    	.data('id', $(e.relatedTarget).data('id'));
		});
		$("[data-ajax=\"true\"]").click(function(e) {
			e.preventDefault();
			var link = $(e.currentTarget);
			var prevHtml = link.html();
			link.html('<i class="fa fa-spinner fa-pulse"></i>');
			$.ajaxSetup(
			{
			    headers:
			    {
			        'X-CSRF-Token': $('input[name="_token"]').val()
			    }
			});
			$.ajax({
				url: '{{URL::to("friends")}}',
				type: 'POST',
				data: {action: link.data('action'), id: link.data('id')},
			})
			.done(function() {
				if(link.data('action')=="remove")
					$('[data-user='+link.data('id')+']').hide(400, function(){ $target.remove(); });
				else if(link.data('action')=="accept")
				{
					var element = $('[data-user='+link.data('id')+']');
					element.hide('normal', function() {
					    element.detach()/*.appendTo($("#friends"))*/;
					    //element.show('normal');
					});

				}
				link.html(prevHtml);
			})
			.fail(function() {
				link.html('<i class="fa fa-times-circle"></i>');
			})
			.always(function() {
				$('.modal').modal('hide');
			});

			
		});
	});
</script>
