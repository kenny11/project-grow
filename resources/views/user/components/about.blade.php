<div class="panel">
	<ul id="aboutTab" class="nav nav-tabs">
		<li class="active"><a href="#about-common" data-toggle="tab">{{ trans('site/profile.about-common') }}</a></li>
		<li class=""><a href="#about-plants" data-toggle="tab">{{ trans('site/profile.about-plants') }}</a></li>
		<li class=""><a href="#about-details" data-toggle="tab">{{ trans('site/profile.about-details') }}</a></li>
	</ul>
	<div id="aboutTabContent" class="tab-content">
		<div class="tab-pane fade active in" id="about-common">
			@include('user.components.about.common')
		</div>
		<div class="tab-pane fade" id="about-plants">
			@include('user.components.about.plants')
		</div>
		<div class="tab-pane fade" id="about-details">
			@include('user.components.about.details')
		</div>
	</div>
</div>