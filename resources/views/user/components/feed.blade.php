@if($user->Posts()->count()!=0)
	{{--@foreach (Auth::user()->Posts()->orderBy('created_at', 'DESC')->get() as $post)--}}
	@foreach ($user->Posts()->orderBy('created_at', 'DESC')->get() as $post)
		<div class="box box-primary box-solid">
			<div class="box-body">
				@include('main.components.posts.'.$post['type'])
			</div>
		</div>
	@endforeach
@else
	<div class="box box-primary box-solid">
		<div class="box-body">
			<h5>{{ trans('site/post.nothing-to-show') }}</h5>
		</div>
	</div>
@endif