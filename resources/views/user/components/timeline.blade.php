<div class="timeline">
	  <dl>
		  <dt>Apr 2014</dt>
		  <dd class="pos-right clearfix">
			  <div class="circ"></div>
			  <div class="time">Apr 14</div>
			  <div class="events">
				  <div class="pull-left">
					  <img src="" class="events-object img-rounded">
				  </div>
				  <div class="events-body">
					  <h4 class="events-heading">Bootstrap</h4>
					  <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica.</p>
				  </div>
			  </div>
		  </dd>
		  <dd class="pos-left clearfix">
			  <div class="circ"></div>
			  <div class="time">Apr 10</div>
			  <div class="events">
				  <div class="pull-left">
					  <img src="" class="events-object img-rounded">
				  </div>
				  <div class="events-body">
					  <h4 class="events-heading">Bootflat</h4>
					  <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica.</p>
				  </div>
			  </div>
		  </dd>
		  <dt>Mar 2014</dt>
		  <dd class="pos-right clearfix">
			  <div class="circ"></div>
			  <div class="time">Mar 15</div>
			  <div class="events">
				  <div class="pull-left">
					  <img src="" class="events-object img-rounded">
				  </div>
				  <div class="events-body">
					  <h4 class="events-heading">Flat UI</h4>
					  <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica.</p>
				  </div>
			  </div>
		  </dd>
		  <dd class="pos-left clearfix">
			  <div class="circ"></div>
			  <div class="time">Mar 8</div>
			  <div class="events">
				  <div class="pull-left">
					  <img src="" class="events-object img-rounded">
				  </div>
				  <div class="events-body">
					  <h4 class="events-heading">UI design</h4>
					  <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica.</p>
				  </div>
			  </div>
		  </dd>

	  </dl>
  </div>