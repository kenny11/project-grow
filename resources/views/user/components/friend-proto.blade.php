<div class="user-proto-box">
	<div class="user-proto-image">
		<img class="profile-user-img img-responsive img-rounded" src="{{asset($friend->profileImagePath())}}" alt="User profile picture">
	</div>
	<div class="text-center user-proto-name">
		<span>
			<a href="{{URL::to($friend->id)}}">{{$friend->first_name}} {{$friend->last_name}}</a>
		</span>
	</div>	
	<div class="user-proto-controll">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		@if(isset($friendRequest))
			<a type="button" class="btn btn-danger" data-ajax="true" data-id="{{$friend->id}}" data-action="remove">
				<i class="fa fa-times"></i>
			</a>
			<a type="button" class="btn btn-success" data-ajax="true" data-id="{{$friend->id}}"  data-action="accept">
				<i class="fa fa-check"></i>
			</a>
		@else
			<a type="button" class="btn btn-danger" data-toggle="modal"
			 data-target="#confirm-delete" data-action="remove" data-id="{{$friend->id}}">
				<i class="fa fa-times"></i>
			</a>
		@endif
	</div>
</div>