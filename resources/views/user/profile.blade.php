@extends('main.components.app')
@section('styles')
	@parent
	<style type="text/css">
	.panel {
	  background-color: #A0D468;
	  border: none;
	  border-bottom: 2px solid #8CC152;
	}
	.panel .list-group {
	  -webkit-box-shadow: none;
	     -moz-box-shadow: none;
	          box-shadow: none;
	}
	.profile-img-big{
		overflow: hidden;
		height: 320px;
	}
	.image-lg{
		width: 100%;
		z-index: -1;
	}
	.profile-info{
		position: absolute;
		z-index: 10;
		top: 260px;
		width: 270px;
	}
	.profile-info .box{
		margin-bottom: 10px;
	}
	.profile-user-img{
		padding: 0px;
	}
	.profile-edit{
		position: absolute;
		right: 10px;
		top: 10px;
	}
	.editing img:hover{
		-webkit-filter: drop-shadow(8px 8px 10px  #AAB2BD);
		filter: drop-shadow(8px 8px 10px  #AAB2BD);
	}
	@media (max-width: 1200px) {
		.profile-info{
			width: 250px;
		}
	}
	@media (max-width: 992px) {
		.profile-info{
			margin-top: 10px;
			top: 0px;
			position: relative;
			width: 100%;
		}
	}
	.modal-header{
		border-bottom: 1px solid gray;
	}
	.editing > .editable{
		border-bottom: 2px solid #3c8dbc;
	}
	</style>
@endsection
@section('content')
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="gridSystemModalLabel">
					{{ trans('site/modal.uploading_header') }}
				</h4>
			</div>
			<div class="modal-body">
				{{ trans('site/modal.uploading_main') }}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">
					{{ trans('site/modal.close') }}
				</button>
			</div>
		</div>
	</div>
</div>
<div class="profile-img-big">
	<img align="left" class="image-lg" src="{{asset('zahlavi.jpg')}}" alt="Profile image example">
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-2 profile-info">
			<div class="box box-primary box-solid">
				<div class="box-body box-profile">
					@if(Helper::isUserSame($user))
					<div class="profile-edit" id="cog">
						<a href="#" class="pull-right btn-box-tool"><i class="fa fa-cog fa-lg"></i></a>
					</div>
					<div class="profile-edit" id="confirm">
						<a href="#" class="pull-right btn-box-tool controll submit"><i class="fa fa-check fa-lg"></i></a>
						<a href="#" class="pull-right btn-box-tool controll"><i class="fa fa-times fa-lg"></i></a>
					</div>

					@endif

					@if(Helper::isUserSame($user))
						<form action="{{ URL::to('/user/changeProfile') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data" data-ajax="true" data-callback="changeProfileImage">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="editType" value="profileImage">
							<input type="file" name="image" style="display: none">
						</form>
					@endif
					<div style="margin: auto;padding: calc;position: relative;display: table;">
						<div style="position: relative;display: inline-block;">
							<img class="profile-user-img img-responsive img-rounded" src="{{asset($user->profileImagePath())}}" alt="User profile picture">
							@if(Helper::isUserSame($user))
								<a href="#" id="imageChange" class="pull-right btn-box-tool" style="top: 0px;right: 0px;position: absolute;">
									<i class="fa fa-cog fa-lg"></i>
								</a>
								<form action="{{URL::to('user/changeProfile')}}" method="POST" accept-charset="utf-8">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="hidden" name="editType" value="profileInfo">
								</form>
							@endif
						</div>
					</div>
					<h3 class="profile-username text-center">
						{{Helper::userFullName($user)}}
					</h3>
					<p class="text-muted text-center editable" data-type="job">
						@if(Auth::check())
							{{$user->job}}
						@else
							{{ trans('site/right-nav.not_logged_in') }}
						@endif
					</p>
					@if(Auth::check())
						<ul class="list-group list-group-unbordered">
							<li class="list-group-item">
								<a href="friends" title="">
									<b>{{ trans('site/right-nav.friends') }}</b>
									<span class="pull-right">{{$user->friendsCount()}}</span>
								</a>
							</li>
						</ul>
						@if(!Helper::isUserSame($user))
							@if(!$user->friendRequestWith(Auth::user()->id)->isEmpty())
								<a href="#" class="btn btn-default btn-block">
									<b>{{ trans('site/right-nav.pending-request') }}&nbsp;
										<i class="fa fa-user-plus"></i>
									</b>
								</a>
							@elseif(!$user->friendWith(Auth::user()->id)->isEmpty())
								<a href="#" class="btn btn-default btn-block">
									<b>{{ trans('site/right-nav.already-friends') }}&nbsp;
										<i class="fa fa-user-plus"></i>
									</b>
								</a>
							@else
								<a href="{{URL::to('friends/add/'.$user->id)}}" class="btn btn-primary btn-block">
									<b>{{ trans('site/right-nav.add_as_friend') }}&nbsp;
										<i class="fa fa-user-plus"></i>
									</b>
								</a>
							@endif
						@endif
					@endif
				</div>
			</div>
			@if(Auth::check())
				<div class="box box-success box-solid">
					<div class="box-header with-border">
						<h3 class="box-title">{{ trans('site/right-nav.about_me') }}</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						@if($user->skills!="")
							<strong><i class="fa fa-map-marker margin-r-5"></i> {{ trans('site/right-nav.location') }}</strong>
							<p class="text-muted editable" data-type="location">{{$user->location}}</p>
						@endif
						@if($user->skills!="")
							<hr>
							<strong><i class="fa fa-pencil margin-r-5"></i> {{ trans('site/right-nav.skills') }}</strong>
							<p>
								@foreach (explode(',',$user->skills) as $skill)
									<span class="label label-success">
									{{$skill}}
									</span>&nbsp;
								@endforeach
							</p>
						@endif
						@if($user->notes!="")
							<hr>
							<strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
							<p class="editable" data-type="notes">{{$user->notes}}</p>
						@endif
					</div>
				</div>
			@endif
		</div>
	</div>	
</div>
<div class="panel">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-3">
				<ul id="mainTab" class="nav nav-tabs">
					<li class="active"><a href="#activity" data-toggle="tab">{{ trans('site/profile.activity') }}</a></li>
					<li class="disabled">
						<a href="#timeline" disabled>
							{{ trans('site/profile.timeline') }}
						</a>
					</li>
					<li class="disabled">
						<a href="#about" disabled>
							{{ trans('site/profile.about') }}
						</a>
					</li>
					<!--
					<li class="disabled">
						<a href="#timeline" data-toggle="tab" disabled>
							{{ trans('site/profile.timeline') }}
						</a>
					</li>
					<li class="disabled">
						<a href="#about" data-toggle="tab" disabled>
							{{ trans('site/profile.about') }}
						</a>
					</li>
					-->
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div id="mainTabContent" class="tab-content col-md-8 col-md-offset-3">
			<div class="tab-pane fade active in" id="activity">
				@include('user.components.feed')
			</div>
			<div class="tab-pane fade" id="timeline">
				@include('user.components.timeline')
			</div>
			<div class="tab-pane fade" id="about">
				@include('user.components.about')
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	@parent
	<script type="text/javascript">
		$(document).ready(function(){
			var editable = false;
			
			$("#confirm").hide();
			function enableEdit(e)
			{
				e.preventDefault();
				editElements = $(".editable")
				if(!editable)
				{
					editElements.wrap("<a href='#' class='editing'></a>");
					$('.editing').click(edit);
					$("#cog").hide();
					$("#confirm").show();
				}
				else
				{
					$.each(editElements, function(index, val) {
						var input = $($(val).children('input'));
						var text = input.attr('placeholder');
						if(!text)
							return;
						var parent = $(input.parent());
						input.remove();
						parent.append(text);
					});
					editElements.unwrap();
					$("#confirm").hide();
					$("#cog").show();
				}
				editable = !editable;
			}
			function edit(e)
			{
				e.preventDefault();
				var target = $(e.target)[0];
				$(e.target).off("click");
				var textEl = $(target.firstChild);
				var textDa = textEl[0].data.trim();
				textEl.remove();
				$(target).append('<input class="form-control" type="text" name="info['+$(e.target).data('type')+']" placeholder="'+textDa+'">');
			}
			function imageUpload(e)
			{
				e.preventDefault();
				$("input[name='image']").click();
			}
			$("input[name='image']").change(function(){
				$($(this)[0].form).submit();
				$(".modal").modal('show');
			})
			$(".controll.submit").click(function(e) {
				e.preventDefault();
				var form = $("input[value='profileInfo']").parent("form");
				$.each($("input[name*='info']"), function(index, val) {
					var input = $(val);
					form.append('<input type="hidden" name="'+input.attr('name')+'" value="'+input.val()+'">')
				});
				$("input[value='profileInfo']").parent("form").submit();
			});
			$("#imageChange").click(imageUpload);
			$('.editing').click(edit);
			$(".profile-edit").click(enableEdit);
		})
	</script>
	<script type="text/javascript">
		function changeProfileImage(data)
		{
			$(".modal").modal('hide');
			$("img.profile-user-img").first().attr("src", "images/"+data.data+".jpg");
			$(".user-block img").each(function(index, el) {
				$(el).attr("src", "images/"+data.data+".jpg");
			});
		}
	</script>
@endsection