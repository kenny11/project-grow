<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2016/02/15 15:14:53 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\posts\\create.blade.php
  'BTN-send-image' => 'Share',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\posts\\create.blade.php
  'BTN-send-post' => 'Send',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\posts\\bottom-bar.blade.php
  'comments' => 'Comments',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\posts\\create.blade.php
  'create-album' => 'Create album',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\posts\\create.blade.php
  'create-imagepost-placeholder' => 'What\'s on your mind',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\list.blade.php
  'create-post-placeholder' => 'What\'s on your mind',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\posts\\comments.blade.php
  'edited' => 'Edited',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\posts\\bottom-bar.blade.php
  'like' => 'Likes',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\user\\components\\feed.blade.php
  'nothing-to-show' => 'Sorry, but there is nothing to show :(',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\posts\\bottom-bar.blade.php
  'share' => 'Share',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\posts\\create.blade.php
  'share-image' => 'Share new image',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\posts\\create.blade.php
  'share-text' => 'Create new post',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\posts\\author.blade.php
  'shared-publicly' => 'Shared publicly',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\posts\\comments.blade.php
  'type-comment' => 'New comment',
  //================================== Obsolete strings ==================================//
  'time-seconds' => ':second second ago|:second seconds ago',
  'time-minute' => ':minute minute ago|:minute minutes ago',
  'time-hour' => ':hour hour ago|:hour hours ago',
  'time-day' => ':day day ago|:day days ago',
  'time-week' => ':week week ago|:week weeks ago',
  //'time-month' => ':month month ago|:month months ago',
  'time-year' => ':year year ago|:year years ago',
  
  

);