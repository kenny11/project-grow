<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2016/03/17 11:05:52 
*************************************************************************/

return array (
  //==================================== Translations ====================================//
  'BTN-send-image' => 'Share',
  'BTN-send-post' => 'Send',
  'comments' => 'Comments',
  'create-album' => 'Create album',
  'create-imagepost-placeholder' => 'What\'s on your mind',
  'create-post-placeholder' => 'What\'s on your mind',
  'edited' => 'Edited',
  'like' => 'Likes',
  'nothing-to-show' => 'Sorry, but there is nothing to show :(',
  'share' => 'Share',
  'share-image' => 'Share new image',
  'share-text' => 'Create new post',
  'shared-publicly' => 'Shared publicly',
  'type-comment' => 'New comment',
  //================================== Obsolete strings ==================================//
  'time-day' => ':day day ago|:day days ago|:day days ago',
  'time-hour' => ':hour hour ago|:hour hours ago|:hour hours ago',
  'time-minute' => ':minute minute ago|:minute minutes ago|:minute minutes ago',
  'time-seconds' => ':second second ago|:second seconds ago|:second seconds ago',
  'time-week' => ':week week ago|:week weeks ago|:week weeks ago',
  'time-month' => ':month month ago|:month months ago|:month months ago',
  'time-year' => ':year year ago|:year years ago|:year years ago',
  
);