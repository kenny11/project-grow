<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2016/02/15 15:14:53 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\nav2.blade.php
  'header' => 'Server under maintenance',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\maintenance.blade.php
  'main_text' => 'Server is now under maintenance mode',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\maintenance.blade.php
  'try_again' => 'Please try it again later',
);