<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2016/02/15 15:14:53 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\dashboard.blade.php
  'dashboard_page' => 'Dashboard',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\develop.blade.php
  'dev_page' => 'Develop',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\components\\header.blade.php
  'header-large' => 'Chilli',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\components\\header.blade.php
  'header-mini' => 'CHLi',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\components\\sidebar.blade.php
  'nav-dashboard' => 'Dashboard',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\components\\sidebar.blade.php
  'nav-develop' => 'Develop',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\components\\sidebar.blade.php
  'nav-main' => 'Main',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\components\\sidebar.blade.php
  'nav-statistics' => 'Statistics',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\components\\sidebar.blade.php
  'nav-users' => 'Users',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\components\\sidebar.blade.php
  'nav-users-overview' => 'Overview',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\components\\sidebar.blade.php
  'nav-users-permissions' => 'Permissions',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\users\\permissions.blade.php
  'permissions_page' => 'Permissions',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\statistics.blade.php
  'statistics_page' => 'Statistics',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\users\\overview.blade.php
  'users_overview_page' => 'Overview',
);