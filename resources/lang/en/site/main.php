<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2016/02/15 15:14:53 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\nav2.blade.php
  'nav-messages-showall' => 'Show all messages',
  //==================================== Translations ====================================//
  'nav-messages' => '{0} You dont have any new messages|{1} You have :count new message|You have :count new messages',
);