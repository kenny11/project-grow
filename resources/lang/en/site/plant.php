<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2016/02/15 15:14:53 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\add-inc.blade.php
  'add-header' => 'Add new plant',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\add-inc.blade.php
  'add-plant' => 'Plant',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'browse' => 'Browse',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'color' => 'Color',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'color-aqua' => 'Aqua',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'color-bittersweet' => 'Bittersweet',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'color-mint' => 'Mint',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'color-pink-rose' => 'Pink rose',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'color-sunflower' => 'Sunflower',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\add-inc.blade.php
  'create-BTN' => 'Create',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'description' => 'Description',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'details-header' => 'Details',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'image' => 'Image',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'name' => 'Name',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'preview' => 'Preview',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'recomended-image-size' => 'Recomended image size is: ',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'update-BTN' => 'Update',
);