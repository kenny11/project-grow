<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2016/03/17 11:05:52 
*************************************************************************/

return array (
  //==================================== Translations ====================================//
  'BTN-send-image' => 'Sdílet',
  'BTN-send-post' => 'Sdílet',
  'comments' => 'Komentáře',
  'create-album' => 'Vytvořit album',
  'create-imagepost-placeholder' => 'Nějaký popisek k vašemu obrázku',
  'create-post-placeholder' => 'Na co právě myslíte?',
  'edited' => 'Upraveno',
  'like' => 'Like',
  'nothing-to-show' => 'Nic k zobrazení',
  'share' => 'Sdílet',
  'share-image' => 'Sdílet obrázek',
  'share-text' => 'Sdílet text',
  'shared-publicly' => 'Veřejné',
  'type-comment' => 'Napište komentář',
  //================================== Obsolete strings ==================================//
  'time-day' => 'Před :day dnem|Před :day dny',
  'time-hour' => 'Před :hour hodinou|Před :hour hodinami',
  'time-minute' => 'Před :minute minutou|Před :minute minutami',
  'time-seconds' => 'Před :second vteřinou|Před :second vteřinami',
  'time-week' => 'Před :week týdnem|Před :week týdny',
  'time-month' => 'There is one apple|There are %count% apples',
  'time-year' => 'Před :year rokem|Před :year lety',
  
);