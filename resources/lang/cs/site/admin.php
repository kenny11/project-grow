<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2016/02/15 15:14:53 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\dashboard.blade.php
  'dashboard_page' => 'Hlavní stránka',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\develop.blade.php
  'dev_page' => 'Vyvojář',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\components\\header.blade.php
  'header-large' => 'Chilli',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\components\\header.blade.php
  'header-mini' => 'CHLi',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\components\\sidebar.blade.php
  'nav-dashboard' => 'Hlavní stránka',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\components\\sidebar.blade.php
  'nav-develop' => 'Vyvojář',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\components\\sidebar.blade.php
  'nav-main' => 'Hlavní',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\components\\sidebar.blade.php
  'nav-statistics' => 'Statistiky',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\components\\sidebar.blade.php
  'nav-users' => 'Uživatelé',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\components\\sidebar.blade.php
  'nav-users-overview' => 'Přehled uživatelů',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\components\\sidebar.blade.php
  'nav-users-permissions' => 'Oprávnění uživatelů',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\users\\permissions.blade.php
  'permissions_page' => 'Oprávnění uživatelů',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\statistics.blade.php
  'statistics_page' => 'Statistiky',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\admin\\users\\overview.blade.php
  'users_overview_page' => 'Přehled uživatelů',
);