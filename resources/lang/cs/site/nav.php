<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2016/02/15 15:14:53 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\nav-menu.blade.php
  'administration' => 'Administrace',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\nav-menu.blade.php
  'feed' => 'Příspěvky',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\user\\components\\friends.blade.php
  'friends' => 'Přátelé',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\nav-menu.blade.php
  'messages' => 'Zprávy',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\nav-menu.blade.php
  'navigation' => 'Navigace',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\nav-menu.blade.php
  'plant-add' => 'Přidat rostlinu',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\nav-menu.blade.php
  'plant-list' => 'Seznam rostlin',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\nav-menu.blade.php
  'plants' => 'Rostliny',
);