<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2016/02/15 15:14:53 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\add-inc.blade.php
  'add-header' => 'Přidat rostlinu',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\add-inc.blade.php
  'add-plant' => 'Přidat rostlinu',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'browse' => 'Hledat',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'color' => 'Barva',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'color-aqua' => 'Světle modrá',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'color-bittersweet' => 'Oranžová',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'color-mint' => 'Tyrkysová',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'color-pink-rose' => 'Růžová',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'color-sunflower' => 'Žlutá',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\add-inc.blade.php
  'create-BTN' => 'Přidat',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'description' => 'Popisek',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'details-header' => 'Detaily',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'image' => 'Obrázek',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'name' => 'Jméno',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'preview' => 'Ukázka',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'recomended-image-size' => 'Doporučená velikost obrázku',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\components\\details-inc.blade.php
  'update-BTN' => 'Aktualizovat',
);