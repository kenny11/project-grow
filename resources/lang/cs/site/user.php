<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2016/02/15 15:14:53 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\welcome.blade.php
  'BTN-login' => 'Přihlásit',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\welcome.blade.php
  'BTN-register' => 'Registrovat',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\auth\\reset.blade.php
  'BTN-reset-password' => 'Resetovat heslo',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\auth\\password.blade.php
  'BTN-send-password-reset' => 'Odeslat nové heslo',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\auth\\register.blade.php
  'already-have-account' => 'Toto jméno je již registrováno',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\auth\\changepassword.blade.php
  'change_password' => 'Změnit heslo',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\welcome.blade.php
  'confirm-password' => 'Potvrdit heslo',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\welcome.blade.php
  'e-mail' => 'E-mail',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\auth\\reset.blade.php
  'e_mail' => 'E-mail',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\welcome.blade.php
  'forgot-password' => 'Zapomenuté heslo',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\auth\\login.blade.php
  'login' => 'Přihlásit',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\auth\\register.blade.php
  'name' => 'Jméno',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\auth\\login.blade.php
  'not-member' => 'Ještě nemáte účet?',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\welcome.blade.php
  'password' => 'Heslo',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\auth\\changepassword.blade.php
  'password_confirmation' => 'Znovu heslo pro potvrzení',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\auth\\register.blade.php
  'register' => 'Registrace',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\welcome.blade.php
  'remember-me' => 'Pamatovat si mě',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\auth\\reset.blade.php
  'reset-password' => 'Obnovit heslo',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\auth\\changepassword.blade.php
  'submit' => 'Odeslat',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\welcome.blade.php
  'username' => 'Uživatelské jméno',
);