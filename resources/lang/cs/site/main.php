<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2016/02/15 15:14:53 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\nav2.blade.php
  'nav-messages-showall' => 'Ukaž vše',
  //==================================== Translations ====================================//
  'nav-messages' => '{0} Nemáte žádné nové zprávy|{1} Máte :count novou zprávu|Máte :count nových zpráv',
);