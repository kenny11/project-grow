<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2016/02/15 15:14:53 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\user\\profile.blade.php
  'close' => 'Zavřít',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\user\\components\\friends.blade.php
  'delete' => 'Smazat',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\user\\components\\friends.blade.php
  'delete_header' => 'Smazat',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\user\\components\\friends.blade.php
  'delete_main' => 'Opravdu si to přejete smazat?',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\list.blade.php
  'share' => 'Sdílet',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\list.blade.php
  'share_header' => 'Sdílet',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\plants\\list.blade.php
  'share_plant' => 'Text k vašemu příspěvku',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\user\\profile.blade.php
  'uploading_header' => 'Nahrávám',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\user\\profile.blade.php
  'uploading_main' => 'Probíhá nahrávání, prosím vyčkejte pár vteřin',
);