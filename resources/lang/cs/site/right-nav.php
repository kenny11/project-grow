<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2016/02/15 15:14:53 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\user\\profile.blade.php
  'about_me' => 'O mně',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\user\\profile.blade.php
  'add_as_friend' => 'Přidat přítele',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\user\\profile.blade.php
  'already-friends' => 'Přátelé',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\user\\profile.blade.php
  'friends' => 'Přátelé',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\user\\profile.blade.php
  'location' => 'Lokace',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\user\\profile.blade.php
  'not_logged_in' => 'Nepřihlášen',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\user\\profile.blade.php
  'pending-request' => 'Žádost odeslána',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\main\\components\\user-info.blade.php
  'profile' => 'Profil',
  // Defined in file F:\\Maturitka\\Xampp PHP7\\htdocs\\Moje\\Chilli\\resources\\views\\user\\profile.blade.php
  'skills' => 'Schopnosti',
);