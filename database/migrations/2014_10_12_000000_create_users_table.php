<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('token')->unique();
            $table->string('first_name');
            //$table->string('second_name');
            $table->string('last_name');
            $table->string('profileImage_name')->index();
            $table->foreign('profileImage_name')->references('name')->on('images')->onDelete('cascade');
            $table->string('job');
            $table->string('degree');

            $table->string('location');
            $table->string('skills');
            $table->string('notes');
            //$table->string('achievements');

            $table->enum('sex', ['male', 'female', 'other'])->default('other');
            $table->json('publicity');
            
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
