<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserInformations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_informations', function (Blueprint $table) {
            $table->increments('id');            
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('first_name');
            //$table->string('second_name');
            $table->string('last_name');
            $table->string('job');
            $table->string('degree');

            $table->string('location');
            $table->string('skills');
            $table->string('notes');
            //$table->string('achievements');

            $table->enum('sex', ['male', 'female', 'other'])->default('other');
            $table->json('publicity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_informations');
    }
}
