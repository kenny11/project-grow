<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);
        DB::table('plants')->insert([
            'name' => str_random(10),
            'user_id' => 1,
            'description' => '',
            'image_name' => 'tn1gsk7lc4.jpg',
            'color' => 'f12a3b'
        ]);
        Model::reguard();
    }
}
